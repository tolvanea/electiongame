#include "agent.h"
#include "location.h"
#include "player.h"
#include "game.h"
//#include "runner.h"
//#include "manualcontrol.h"

#include <QSignalSpy>
#include <QtTest>
#include <QString>
#include <memory>
#include <iostream>

#include "stateexception.h"

using Interface::Location;
using Interface::Player;
using Interface::Game;
using std::shared_ptr;
using std::make_shared;

class TestAgent : public QObject
{
    Q_OBJECT

public:
    TestAgent();

private Q_SLOTS:
    /**
     * @brief agentCreation tests the agent constructor, and getters and setters
     */
    void agentCreation();

    /**
     * @brief agentOwner tests owner setters and getters
     */
    void agentOwner();

    /**
     * @brief agentLocation tests location setting and getting (with owner)
     */
    void agentLocation();

    /**
     * @brief agentExceptions tests if right exception is thrown when invariant breaks
     */
    void agentExceptions();

    /**
     * @brief agentExceptions_data is data storage for 'agentExceptions()'
     */
    void agentExceptions_data();

    /**
     * @brief agentConnections tests setting and modifying agent connections
     */
    void agentConnections();

    /**
     * @brief agentConnections_data is data-function for 'agentConnections()' Qt tests
     */
    void agentConnections_data();
};

TestAgent::TestAgent()
{
}

void TestAgent::agentCreation()
{
    Agent agent("A name","A title", false, 1);
    QCOMPARE(agent.name(), QString("A name"));
    QCOMPARE(agent.title(), QString("A title"));
    QCOMPARE(agent.isCommon(), false);
    QCOMPARE(agent.specificLocationId(), static_cast<unsigned short>(1));
    QCOMPARE(agent.typeName(), QString("Agent"));

    Agent agent2("","äöå(/!&)=)ÄÖÅ*¨23#¤%& sudo rm -rf / && :(){ :|: & };:", true);
    QCOMPARE(agent2.name(), QString(""));
    QCOMPARE(agent2.title(), QString("äöå(/!&)=)ÄÖÅ*¨23#¤%& sudo rm -rf / && :(){ :|: & };:"));
    QCOMPARE(agent2.isCommon(), true);
    QCOMPARE(agent2.specificLocationId(), static_cast<unsigned short>(0));
    QCOMPARE(agent2.typeName(), QString("Agent"));

}

void TestAgent::agentOwner()
{

    shared_ptr<Game> game = make_shared<Game>();
    shared_ptr<Player> player1 = game->addPlayer("Player's name");
    shared_ptr<Player> player2 = game->addPlayer("Other player's name");

    Agent agent1("Some name","Some title", false, 1);
    agent1.setOwner(player1);
    QCOMPARE(player1, agent1.owner().lock());

    Agent agent2("Some name","Some title", true);
    agent2.setOwner(player1);
    QCOMPARE(player1, agent2.owner().lock());

    agent1.setOwner(player2);
    QCOMPARE(player2, agent1.owner().lock());


}

void TestAgent::agentLocation()
{

    shared_ptr<Game> game = make_shared<Game>();
    shared_ptr<Player> player = game->addPlayer("Player's name");

    shared_ptr<Location> location = make_shared<Location>(1, "Name of location");
    location->initialize();
    game->addLocation(location);


    shared_ptr<Agent> agent = make_shared<Agent>("Some name","Some title", false, 1);
    agent->setOwner(player);
    location->sendAgent(agent);

    QCOMPARE(location, agent->placement().lock());
}

void TestAgent::agentExceptions()
{
    QFETCH(unsigned short, connections_set);
    QFETCH(short, connections_modify);

    shared_ptr<Game> game = make_shared<Game>();
    shared_ptr<Player> player = game->addPlayer("Player's name");

    shared_ptr<Location> location = make_shared<Location>(1, "Name of location");
    location->initialize();
    game->addLocation(location);


    shared_ptr<Agent> agent1 = make_shared<Agent>("Some name","Some title", false, 1);
    shared_ptr<Agent> agent2 = make_shared<Agent>("Some name","Some title", false, 1);
    agent1->setOwner(player);
    agent2->setOwner(player);
    location->sendAgent(agent1);
    location->sendAgent(agent2);

    QVERIFY_EXCEPTION_THROWN(agent1->setConnections(connections_set),
                             Interface::StateException);
    QVERIFY_EXCEPTION_THROWN(agent2->modifyConnections(connections_modify),
                             Interface::StateException);
}

void TestAgent::agentExceptions_data()
{
    QTest::addColumn<unsigned short>("connections_set");
    QTest::addColumn<short>("connections_modify");

    QTest::newRow("Too big value")
            << static_cast<unsigned short>(100) << static_cast<short>(100);
    QTest::newRow("Too small value")
            << static_cast<unsigned short>(0) << static_cast<short>(-100);
    QTest::newRow("Over bound by one")
            << static_cast<unsigned short>(11) << static_cast<short>(10);
    QTest::newRow("Under bound by one")
            << static_cast<unsigned short>(0) << static_cast<short>(-1);
}

void TestAgent::agentConnections()
{
    QFETCH(unsigned short, connections_set);
    QFETCH(short, connections_modify);

    shared_ptr<Game> game = make_shared<Game>();
    shared_ptr<Player> player = game->addPlayer("Player's name");

    shared_ptr<Location> location = make_shared<Location>(1, "Name of location");
    location->initialize();
    game->addLocation(location);


    shared_ptr<Agent> agent = make_shared<Agent>("Some name","Some title", false, 1);
    agent->setOwner(player);
    location->sendAgent(agent);

    agent->setConnections(connections_set);
    QVERIFY( (1<=agent->connections()) && (agent->connections()<=10));

    agent->setConnections(1);

    agent->modifyConnections(connections_modify);
    QVERIFY( (1<=agent->connections()) && (agent->connections()<=10));




}

void TestAgent::agentConnections_data()
{
    QTest::addColumn<unsigned short>("connections_set");
    QTest::addColumn<short>("connections_modify");


    QTest::newRow("Valid value")
            << static_cast<unsigned short>(5) << static_cast<short>(1);
    QTest::newRow("Lower bound")
            << static_cast<unsigned short>(1) << static_cast<short>(0);
    QTest::newRow("Upper bound")
            << static_cast<unsigned short>(10) << static_cast<short>(9);

}


QTEST_APPLESS_MAIN(TestAgent)

#include "tst_testagent.moc"
