#include "removeagentaction.h"

#include "actioninterface.h"
#include "agent.h"
#include "player.h"
#include "location.h"
#include "cardinterface.h"

#include "bookkeeping.h"
#include "statswidget.h"

#include <memory>
#include <QDebug>

using std::shared_ptr;
using Interface::CardInterface;

RemoveAgentAction::RemoveAgentAction(std::shared_ptr<Agent> victim, std::shared_ptr<Agent> hacker):
    hacker_(hacker),
    victim_(victim),
    location_(victim->placement().lock())
{

}

bool RemoveAgentAction::canPerform() const
{
    bool db = false; //debug messages

    // There's two options: hacker removes agent OR player removes his own agent

    // if one is removed by hacker
    if (hacker_ != nullptr){
        bool is_hacker = (hacker_->name() == "Hacker");

        std::vector<shared_ptr<CardInterface>> hand_vec = hacker_->owner().lock()->cards();
        bool hacker_in_hand = (std::find(hand_vec.begin(), hand_vec.end(), hacker_) != hand_vec.end());

        bool victim_is_removable = (victim_->title() != "Subordinate");

        bool victim_on_board = (victim_->location().lock() != nullptr);

        bool is_not_own_card = (hacker_->owner().lock() != victim_->owner().lock());

        if (db) qDebug()<<"         <<is_hacker<<hacker_in_hand<<victim_is_removable<<victim_on_board<<is_not_own_card<<hacker_->name(): "<<is_hacker<<hacker_in_hand<<victim_is_removable<<victim_on_board<<is_not_own_card<<hacker_->name();
        return is_hacker && hacker_in_hand && victim_is_removable && victim_on_board && is_not_own_card;
    }
    else{
        bool victim_is_removable = (victim_->title() != "Subordinate");
        if (db) qDebug()<<"        victim_is_removable: (victim_->title() != Subordinate)"<<victim_is_removable;
        return victim_is_removable;
    }
}

void RemoveAgentAction::perform()
{
    bool db =  true; //debug messages
    victim_->invariant2();

    // it's action caused by hacker
    if (hacker_ != nullptr){
        hacker_->invariant2();
        hacker_->owner().lock()->playCard(hacker_);
        victim_->placement().lock()->removeAgent(victim_);
        Bookkeeping::B.getStatsWidget()->showText(0, "Removed "+victim_->name()+ " from");
        Bookkeeping::B.getStatsWidget()->showText(1, "game");
    }
    // player is removing own card
    else {
        // victim is in hand
        if (victim_->placement().lock() == nullptr){
            victim_->owner().lock()->playCard(victim_);
            Bookkeeping::B.getStatsWidget()->showText(0, "Dumped card to thrash");
            Bookkeeping::B.getStatsWidget()->showText(1, "");
        }
        //victim is on board
        else {
            victim_->placement().lock()->removeAgent(victim_);
        }
    }
    if (db) qDebug()<<"        RemoveAgentAction";
}
