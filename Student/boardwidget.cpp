#include "boardwidget.h"

BoardWidget::BoardWidget(QGraphicsScene *scene, std::shared_ptr<DragManager> dragManager, std::shared_ptr<Interface::Game> game) : QGraphicsView(scene)
{
    // setViewport(new QOpenGLWidget);
    setRenderHint(QPainter::Antialiasing, true);

    setMinimumHeight(100);
    setMinimumWidth(100);
    zoomLevel_ = 0.5;
    scale(zoomLevel_, zoomLevel_);

    // setDragMode(QGraphicsView::ScrollHandDrag);

    game_ = game;
    dragManager_ = dragManager;

    width_ = scene->width();
    height_ = scene->height();

    Board *board = new Board(scene, game_);
    scene->addItem(board);

    /*
    QPixmap boardmap;
    boardmap.load("Assets/testboard.gif");
    scene->addPixmap(boardmap);
    */

    const int cardSpaceX = cardWidth_ + 20;
    const int cardSpaceY = cardHeight_ + 20;

    const int cornerDistanceX = 500;
    const int cornerDistanceY = 300;

    const int areaCorners[4][2] = { {cornerDistanceX, cornerDistanceY},
                          {width_-cornerDistanceX-cardWidth_, cornerDistanceY},
                          {width_-cornerDistanceX-cardWidth_, height_-cornerDistanceY-cardHeight_},
                          {cornerDistanceX, height_-cornerDistanceY-cardHeight_} };

    // Normal card slots
    const int slotPositions[4][5][2] = { { {0, 0}, {1, 0}, {2, 0}, {0, 1}, {1, 1} },
                        { {0, 0}, {-1, 0}, {-2, 0}, {0, 1}, {-1, 1} },
                        { {0, 0}, {-1, 0}, {-2, 0}, {0, -1}, {-1, -1} },
                        { {0, 0}, {1, 0}, {2, 0}, {0, -1}, {1, -1} } };

    for (int i = 0; i < 4; i++) {
        std::vector<CardSlot*> vector;
        cardSlots_.push_back(vector);

        for (int j = 0; j < 5; j++) {
            CardSlot *cardSlot = new CardSlot(dragManager_, game_->locations()[i]);
            int xpos = areaCorners[i][0] + slotPositions[i][j][0] * cardSpaceX;
            int ypos = areaCorners[i][1] + slotPositions[i][j][1] * cardSpaceY;
            cardSlot->setPos(xpos, ypos);
            scene->addItem(cardSlot);

            cardSlots_[i].push_back(cardSlot);
        }
    }

    // Destroy slots
    const int discardPositions[4][2] = { {-2, 0}, {2, 0}, {2, 0}, {-2, 0} };

    for (int i = 0; i < 4; i++) {
        DiscardSlot *discardSlot = new DiscardSlot(dragManager_, game_->locations()[i]);
        int xpos = areaCorners[i][0] + discardPositions[i][0] * cardSpaceX;
        int ypos = areaCorners[i][1] + discardPositions[i][1] * cardSpaceY;
        discardSlot->setPos(xpos, ypos);
        scene->addItem(discardSlot);
        discardSlots_.push_back(discardSlot);
    }

    // Deck slots
    const int deckPositions[4][2] = { {-2, 1}, {2, 1}, {2, -1}, {-2, -1} };

    for (int i = 0; i < 4; i++) {
        DeckSlot *deckSlot = new DeckSlot(dragManager_, game_->locations()[i]);
        int xpos = areaCorners[i][0] + deckPositions[i][0] * cardSpaceX;
        int ypos = areaCorners[i][1] + deckPositions[i][1] * cardSpaceY;
        deckSlot->setPos(xpos, ypos);
        scene->addItem(deckSlot);
        deckSlots_.push_back(deckSlot);
    }

    connectionSlot_ = new ConnectionSlot(dragManager_);
    connectionSlot_->setPos(0.43*width_, 0.5*height_);
    scene->addItem(connectionSlot_);

    influenceSlot_ = new InfluenceSlot(dragManager_);
    influenceSlot_->setPos(0.57*width_, 0.5*height_);
    scene->addItem(influenceSlot_);

    // Test slots
    /*
    CardSlot *testSlot = new CardSlot(dragManager_);
    testSlot->setPos(200, 300);
    scene->addItem(testSlot);

    CardSlot *testSlot2 = new CardSlot(dragManager_);
    testSlot2->setPos(800, 200);
    scene->addItem(testSlot2);
    */

    // Test cards
    /*
    Card *testCard = new Card(dragManager_);
    testCard->setPos(500, 500);
    scene->addItem(testCard);

    Card *testCard2 = new Card(dragManager_);
    testCard2->setPos(700, 500);
    scene->addItem(testCard2);
    */

    Bookkeeping::B.setBoardWidget(this);

    show();
}

void BoardWidget::wheelEvent(QWheelEvent *event)
{
    int degrees = event->delta();
    // qDebug() << "Scrolling:" << degrees;
    double newScaler = 1.0 + (degrees/800.0);

    //qDebug() << zoomLevel * newScaler;
    if(newScaler * zoomLevel_ < 0.1 || newScaler * zoomLevel_ > 2.0){
        return;
    } else {
        zoomLevel_ = zoomLevel_ * newScaler;
        scale(newScaler, newScaler);
    }
}

// The pan handling is from
// https://stackoverflow.com/questions/4753681/how-to-pan-images-in-qgraphicsview

void BoardWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        pan_ = true;
        panStartX_ = event->x();
        panStartY_ = event->y();
        setCursor(Qt::ClosedHandCursor);
        event->accept();
        return;
    } else {
        QGraphicsView::mousePressEvent(event);
    }
}

void BoardWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        pan_ = false;
        setCursor(Qt::ArrowCursor);
        event->accept();
        return;
    } else {
        QGraphicsView::mouseReleaseEvent(event);
    }
}

void BoardWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (pan_) {
        horizontalScrollBar()->setValue(horizontalScrollBar()->value() - (event->x() - panStartX_));
        verticalScrollBar()->setValue(verticalScrollBar()->value() - (event->y() - panStartY_));
        panStartX_ = event->x();
        panStartY_ = event->y();
        event->accept();
        return;
    } else {
        QGraphicsView::mouseMoveEvent(event);
    }
}
