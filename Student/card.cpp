#include "card.h"


const std::map<QString, QString> Card::picNames_ = {
    {"Twitter user", ":/twitter.png"},
    {"Selfie guy", ":/selfie_man.png"},
    {"9gag meme", ":/meme.png"},
    {"Cambridge Analytica", ":/cambridge_analytica.png"},
    {"Friendly to children", ":/friendly_to_childs.png"},
    {"Troll", ":/troll.png"},
    {"Hacker", ":/hacker.png"},
    {"Manipulator", ":/manipulator.png"},
    {"Business shark", ":/business_shark.png"},
    {"Consult", ":/consult.png"},
    {"Concession to oil", ":/concession_to_oil.png"},
    {"Emission permission", ":/emission_permission.png"},
    {"Honest intentions", ":/honest_intentions.png"},
    {"Tax deductions", ":/tax_deductions.png"},
    {"Tolerant & Friendly", ":/tolerant_and_friendly.png"},
    {"Chairman's pal", ":/chairmans_pal.png"},
    {"Delegate", ":/delegate.png"},
    {"Promotion", ":/promotion.png"},
    {"Bribed journalist", ":/paid_reporter.png"},
};

const std::map<unsigned short, QString> Card::picSubordinateNames_ = {
    {0, ":/subordinate.png"},
    {1, ":/subordinate_2.png"},
    {2, ":/subordinate_3.png"},
    {3, ":/subordinate_4.png"},
};

const std::map<unsigned short, QString> Card::picSupporterNames_ = {
    {0, ":/supporter_0.png"},
    {1, ":/supporter_1.png"},
    {2, ":/supporter_2.png"},
    {3, ":/supporter_3.png"},
};

Card::Card(std::shared_ptr<DragManager> dragManager)
{
    dragManager_ = dragManager;

    setAcceptedMouseButtons(Qt::LeftButton);
    setAcceptHoverEvents(true);
    setAcceptDrops(true);

    // qDebug() << "Created card" << this << "of type" << type();
}

Card::~Card()
{
    qDebug() << "Destroying card" << this;
    std::shared_ptr<Interface::Game> game = Bookkeeping::B.getGame().lock();
    if(game != nullptr) {
        if(game->active()) {
            if(cardSlot_ != nullptr)
                cardSlot_->setCard(nullptr);
            if(scene() != nullptr)
                scene()->removeItem(this);
        }
    }
}

QRectF Card::boundingRect() const
{
    return QRectF(0, 0, width_, height_);
}

void Card::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    paintBase(painter);

    painter->setBrush(Qt::white);
    painter->drawRect(5, 5, width_-10, height_-10);

    painter->setPen(Qt::black);
    painter->drawText(10, 40, QString("BASE CARD"));
}

void Card::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (canGrab())
        setCursor(Qt::ClosedHandCursor);
    event->accept();
}

void Card::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    invariant();

    // Do not start drag if the mouse hasn't been moved enough
    if (QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton))
        .length() < QApplication::startDragDistance()) {
        return;
    }

    if( canGrab() ) {
        CardSlot *oldSlot = cardSlot_;

        QDrag *drag = new QDrag(event->widget());
        QMimeData *mime = new QMimeData;
        drag->setMimeData(mime);

        mime->setText("card");

        drag->setPixmap(toPixmap());

        hide();
        dragManager_->startDrag(this, true);
        drag->exec();
        dragManager_->endDrag();
        show();
        setCursor(Qt::OpenHandCursor);

        Bookkeeping::B.updateStats();

        qDebug() << "Card movement:" << this << "from" << oldSlot << "to" << cardSlot_;
        if (destroy_) {
            delete this;
        } else {
            // delete this does not stop the function execution and checking
            // the invariant would result in the Q_ASSERT failing since the object no longer exists
            invariant();
        }
    }
}

void Card::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if(canGrab())
        setCursor(Qt::OpenHandCursor);
    event->accept();
}

void Card::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    event->setAccepted(true);
    if (event->mimeData()->text() == "card") {
        Card *card = dragManager_->getCard();
        if (canDrop(card)) {
            dragOver_ = true;
            update();
        }
    }
    invariant();
}

void Card::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
    event->accept();
    dragOver_ = false;
    update();
    invariant();
}

void Card::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    invariant();
    dragOver_ = false;
    if (event->mimeData()->text() == "card") {
        Card *card = dragManager_->getCard();
        qDebug() << "Trying to drop card" << card << "on top of" << this;
        if (canDrop(card)) {
            QString whatToDo = Bookkeeping::B.getValidateActions()->drop();

           if (whatToDo == "delete this card") {
               card->destroy();
           } else if (whatToDo == "delete both cards") {
               card->destroy();
               delete this;
           }
            update();
        }
    }
}

void Card::paintBase(QPainter *painter)
{
    if (painter != nullptr) {
        try {
            painter->setPen(Qt::NoPen);
            painter->setBrush(Qt::red);

            if (dragOver_) {
                painter->setBrush(Qt::red);
            } else {
                painter->setBrush(QColor(130, 143, 255));
            }

            painter->drawRect(boundingRect());

            if (hidden_) {
                //painter->setBrush(QColor(77, 128, 126));
                QBrush brush(QBrush(Qt::DiagCrossPattern));
                brush.setTransform(QTransform().scale(4,4));
                painter->setBrush(brush);
            } else {
                painter->setBrush(Qt::white);
            }
            painter->drawRect(5, 5, width_-10, height_-10);

            // Draw picture and owner
            if (! hidden_) {

                QPixmap pixmap;
                bool found = false;

                if (gameCard_->title() == "Subordinate") {
                    unsigned short playerId = gameCard_->owner().lock()->id();
                    // qDebug() << "Player id:" << playerId;
                    auto search = picSubordinateNames_.find(playerId);
                    if (search != picSubordinateNames_.end()) {
                        pixmap = QPixmap(search->second);
                        found = true;
                    } else {
                        throw "Too many players";
                    }
                } else if (gameCard_->name() == "Supporter"){
                    // This had to be moved to AgentCard since it requires an Agent method
                } else {
                    auto search = picNames_.find(gameCard_->name());
                    if (search != picNames_.end()) {
                        pixmap = QPixmap(search->second);
                        found = true;
                    }
                }

                if (found) {
                    // qDebug() << "Picture found, drawing";
                    painter->drawPixmap(5, 5, width_-10, width_-10, pixmap);
                }

                painter->setPen(Qt::black);
                painter->drawLine(5, 185, width_-5, 185);
            }
        } catch (Interface::GameException e) {
            painter->setPen(Qt::black);
            painter->drawText(10, 40, QString("PAINTING ERROR"));
            qDebug() << "Exception occurred when painting card" << this << e.msg();
        }
    }
}

void Card::invariant() const
{
    if (cardSlot_ != nullptr) {
        // qDebug() << "Invariant: this" << this << "my slot" << cardSlot_;
        // qDebug() << "Card in my slot" << cardSlot_->getCard();
        // qDebug() << " my slot -> card -> slot" << cardSlot_->getCard()->getSlot();

        // Q_ASSERT(cardSlot_->getCard() == this);
        if (cardSlot_->getCard() != this)
            throw Interface::StateException(QString("Card invariant failed"));
    }
}


// Copied from here
// http://blog.embedded.pro/rendering-qgraphicsitem-into-qpixmap/
QPixmap Card::toPixmap()
{
    // Retrieve the bounding rect
    QRect rect = boundingRect().toRect();
    if (rect.isNull() || !rect.isValid()) {
        return QPixmap();
    }

    // Create the pixmap
    QPixmap pixmap(rect.size());
    pixmap.fill(Qt::transparent);

    // Render
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setRenderHint(QPainter::TextAntialiasing, true);
    painter.translate(-rect.topLeft());
    paint(&painter, nullptr, nullptr);
    for (QGraphicsItem* child : childItems()) {
        painter.save();
        painter.translate(child->mapToParent(pos()));
        child->paint(&painter, nullptr, nullptr);
        painter.restore();
    }

    painter.end();

    return pixmap;
}

void Card::setSlot(CardSlot *cardSlot)
{
    cardSlot_ = cardSlot;
}

CardSlot *Card::getSlot() const
{
    return cardSlot_;
}

std::shared_ptr<Interface::CardInterface> Card::getGameCard() const
{
    return gameCard_;
}

bool Card::canGrab() const
{
    try {
        std::shared_ptr<Interface::Game> game = Bookkeeping::B.getGame().lock();
        if (game == nullptr || (! game->active())) {
            return false;
        } else if (cardSlot_ == nullptr) {
            return true;
        } else {
            std::shared_ptr<ValidateActions> va = Bookkeeping::B.getValidateActions();
            va->discardAction();
            bool canGrab = va->canGrab(cardSlot_->getType(), cardSlot_->getLocation(), gameCard_);
            // qDebug() << "Can grab:" << canGrab;
            return canGrab;
        }
    } catch(Interface::GameException e) {
        qDebug() << "Exception occurred when testing card grabbing:" << e.msg();
        return false;
    }
}

bool Card::canDrop(Card *card) const
{
    try {
        shared_ptr<ValidateActions> va = Bookkeeping::B.getValidateActions();
        va->discardAction();
        bool validDrop = va->canDrop(card->getSlot()->getType(), getSlot()->getType(), card->getGameCard()->location().lock(), getGameCard()->location().lock(), card->getGameCard(), getGameCard());
        // qDebug() << "Can drop over card:" << validDrop;
        return validDrop;
    } catch(Interface::GameException e) {
        qDebug() << "Exception occurred when testing card dropping on card:" << e.msg();
        return false;
    }
}

void Card::setHidden(bool hidden)
{
    hidden_ = hidden;
}

/*
void Card::setOriginalLocation(short location)
{
    originalLocation_ = location;
}
*/

void Card::destroy()
{
    destroy_ = true;
}

void Card::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    if(canGrab()) {
        setCursor(Qt::OpenHandCursor);
    }
    event->accept();
}

void Card::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Bookkeeping::B.getValidateActions()->discardAction();
    setCursor(Qt::ArrowCursor);
    event->accept();
}

void TestCard::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::red);

    paintBase(painter);

    if (! hidden_) {
        painter->setPen(Qt::black);
        painter->setBrush(Qt::NoBrush);
        painter->setFont(labelFont_);
        painter->drawText(10, 20, label_);
        painter->drawText(10, 40, QString::number(stat1_));
        painter->drawText(10, 60, QString::number(stat2_));
    }
}

AgentCard::AgentCard(std::shared_ptr<DragManager> dragManager, std::shared_ptr<Agent> agent) : Card::Card(dragManager)
{
    agent_ = agent;
    gameCard_ = std::static_pointer_cast<Interface::CardInterface>(agent);
}

void AgentCard::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    paintBase(painter);

    if(! hidden_) {
        if (gameCard_->name() == "Supporter"){
            QPixmap pixmap;
            bool found = false;
            auto search = picSupporterNames_.find(agent_->specificLocationId());
            if (search != picSupporterNames_.end()) {
                pixmap = QPixmap(search->second);
                found = true;
            }
            if (found) {
                // qDebug() << "Picture found, drawing";
                painter->drawPixmap(5, 5, width_-10, width_-10, pixmap);
            }
        }

        painter->setPen(Qt::black);
        painter->setBrush(Qt::NoBrush);
        painter->setFont(labelFont_);
        painter->drawText(10, 200, agent_->name());
        painter->drawText(10, 220, agent_->typeName());
        painter->drawText(10, 240, agent_->title());
        painter->drawText(10, 260, QString("C") + QString::number(agent_->connections()));

        // Owner
        painter->setFont(labelFont_);
        std::shared_ptr<Interface::Player> owner = gameCard_->owner().lock();
        if (owner != nullptr) {
            painter->setPen(Qt::NoPen);
            QColor color = Bookkeeping::B.playerColors()->at(owner->id());
            painter->setBrush(color);

            painter->drawPie(QRect(width_-55, height_-55, 100, 100), 90*16, 90*16);
            painter->setBrush(Qt::white);
            painter->drawPie(QRect(width_-40, height_-40, 70, 70), 90*16, 90*16);

            painter->setPen(Qt::black);
            painter->drawText(160, 260, QString("P") + QString::number(owner->id() + 1));

        } else {
            painter->drawText(160, 260, QString("P-"));
        }
    }
}

InfluenceCard::InfluenceCard(std::shared_ptr<DragManager> dragManager, std::shared_ptr<Interface::Influence> influence) : Card(dragManager)
{
    influence_ = influence;
    gameCard_ = std::static_pointer_cast<Interface::CardInterface>(influence);
}

void InfluenceCard::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    paintBase(painter);

    if(! hidden_) {
        painter->setPen(Qt::black);
        painter->setBrush(Qt::NoBrush);
        painter->setFont(labelFont_);
        painter->drawText(10, 200, influence_->name());
        painter->drawText(10, 220, influence_->typeName());
        painter->drawText(10, 640, influence_->title());
    }
}
