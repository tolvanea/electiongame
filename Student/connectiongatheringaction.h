#ifndef CONNECTIONGATHERINGACTION_H
#define CONNECTIONGATHERINGACTION_H

#include "actioninterface.h"
#include "agent.h"
#include "player.h"

#include <memory>

/**
 * @file
 * @brief Action class for raising agent's connection.
 */
using Interface::ActionInterface;
using Interface::Location;

/**
 * @brief The ConnectionGatheringAction class implements agent's operation for
 * gatherin connections. Conncetions are agent-specific, and shows "the level of
 * agent". Connections are not lost if agent leaves the location.
 */
class ConnectionGatheringAction: public ActionInterface
{
public:
    /**
     * @brief ConnectionGatheringAction constructor
     * @param shared pointer to agent
     * @pre -
     * @post action created
     */
    ConnectionGatheringAction(std::shared_ptr<Agent> agent);

    /**
     * @brief ~ConnectionGatheringAction destructor
     */
    virtual ~ConnectionGatheringAction() = default;

    /**
     * @brief canPerform checks whether it is possible agent to gather connections
     * @pre -
     * @return true, iff agent on board
     * @post exception gurantee: nothrow
     */
    virtual bool canPerform() const;

    /**
     * @brief perform raises agent's for that location, takes in account
     * agent's current connections.
     * @pre canPerform() == true
     * @post exception gurantee: basic
     */
    virtual void perform();

private:
    std::shared_ptr<Agent> agent_;



};

#endif // CONNECTIONGATHERINGACTION_H
