#ifndef VALIDATEACTIONS_H
#define VALIDATEACTIONS_H

#include "cardinterface.h"
#include "location.h"
#include "actioninterface.h"

#include <memory>

using Interface::CardInterface;
using Interface::Location;
using Interface::ActionInterface;
using std::shared_ptr;

/**
 * @brief The ValidateActions class is for checking wherther card move is legal
 * or not, and then using the same object to perform corresponding action.
 * @code
 * // Example of use
 * ValidateActions va = Bookkeepping::B.getValidateActions();
 * bool valid_grab = va->canGrab(slot_from, location_from, card);
 * if (valid_grab){
 *      valid_drop = va.canDrop(slot_from, slot_to, location_from, location_to,
 *                              card, card_under);
 *      if (valid_drop){
 *          va->drop();
 *      }
 * }
 * va->discardAction(); // Remember always to reset when current move-action is over!
 * @endcodez
 */
class ValidateActions: public QObject
{
    Q_OBJECT
public:

    /**
     * @brief ValidateActions Constructor
     */
    ValidateActions();

    /**
     * @brief canGrab checks whether player is able to grap that card.
     * @param slot_from "hand", "location_slot", "deck"
     * @param location_from the board location card is from
     * @param card shared pointer to card
     * @return true iff player can start dragging
     */
    bool canGrab(QString slot_from,
                 shared_ptr<Location> location_from,
                 shared_ptr<CardInterface> card);

    /**
     * @brief canDrop checks whether player can drop card to that slot
     * @param slot_from card is from: "hand", "location_slot" or "deck"
     * @param slot_to "hand", "location_slot", "influence", "connection", "trash"
     * @param location_from shared pointer to location where which card is from
     * @param location_to shared pointer to location which is underneath the card
     * @param card shared pointer to dragging card
     * @param card_under shared pointer to card that is in slot underneath,
     * if there is no card, nullptr
     * @return true iff player can drop card there
     * @pre canGrab() is true for that card
     * @post if return true, 'drop()' is ready to be called
     */
    bool canDrop(QString slot_from,
                 QString slot_to,
                 shared_ptr<Interface::Location> location_from,
                 shared_ptr<Location> location_to,
                 shared_ptr<CardInterface> card,
                 shared_ptr<CardInterface> card_under);

    /**
     * @brief drop Drops the card in slot, and does correspondind actions.
     * @return Qstring what happens to the card: "move", "stay", "delete this card", "delete both cards"
     * @pre canDrop() is true, and not expired by 'discardAction()'
     * @post action is performed by GameWindow and runner
     */
    QString drop();

    /**
     * @brief discardAction resets the object to null state. This function should
     * be called when grabbed card is not anymore over slot.
     * @post object has been reset
     */
    void discardAction();

signals:
    void processAction(shared_ptr<ActionInterface> action, bool no_turn_count);

private:
    shared_ptr<ActionInterface> current_action_;
    bool no_turn_count_;
    QString card_state_change_;
    bool perform_no_action_;

};
#endif // VALIDATEACTIONS_H


