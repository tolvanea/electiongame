#ifndef AGENT_H
#define AGENT_H

#include "agentinterface.h"
#include "game.h"
#include "gameexception.h"
#include "stateexception.h"



#include <memory>

/**
 * @file
 * @brief Defines agent-card
 */

namespace Interface
{
class Player;
}

/**
 * @brief Agent -class defines Agent-card, it is inherited from AgentInterface,
 * which is inherited from CardInterface.
 *
 * @invariant If (placement().lock() != nullptr) then (connections() > 0).
 * In other words, if agent is on board, it has connections
 * All other invariants are under 'invariant()' member-functions
 */
class Agent: public Interface::AgentInterface
{
public:
    /**
     * @brief Agent constructor
     * @pre -
     * @param name personal name of the agent
     * @param title title of profession
     * @post New agent-card created that has no owner nor location assigned.
     */
    explicit Agent(const QString& name, const QString& title, bool common, unsigned short id = 0);

    /**
     * @brief ~Agent virtual destructor
     */
    virtual ~Agent() = default;

    /**
     * @brief typeName tells type of card
     * @pre -
     * @return kortin tyyppi
     * @post Exception guarantee: nothrow
     */
    virtual QString typeName() const;

    /**
     * @brief name returns personal name of the agent
     * @pre -
     * @return name in QString
     * @post Exception guarantee: nothrow
     */
    virtual QString name() const;

    /**
     * @brief title returns title of agent
     * @pre -
     * @return title in QString
     * @post Exception guarantee: nothrow
     */
    virtual QString title() const;

    /**
     * @brief location tells on which location agent is
     * @pre -
     * @return weak pointer to current location, or nullptr, if it isn't on any
     * location
     * @post Exception guarantee: nothrow
     */
    virtual std::weak_ptr<Interface::Location> location() const;

    /**
     * @brief owner tells the player who owns agent
     * @pre -
     * @return weak pointer to player, or nullptr if no one owns agent
     * @post Exception guarantee: nothrow
     */
    virtual std::weak_ptr<Interface::Player> owner() const;

    /**
     * @brief setOwner set player to agent's owner
     * @pre -
     * @param owner weak pointer to owner, or nullptr, if no-one owns agent
     * @post card has new owner
     * @post Exception guarantee: nothrow
     */
    virtual void setOwner(std::weak_ptr<Interface::Player> owner);

    /**
     * @brief isCommon tells whether agent is generic, i.e it can be sent on all
     * locations
     * @pre -
     * @return true iff agent is 'Avustaja'
     * @post Exception guarantee: nothrow
     */
    virtual bool isCommon() const;

    /**
     * @brief specificLocationId tells non-generic agent's specific location
     * @pre -
     * @return location id
     * @post Exception guarantee: nothrow
     */
    virtual unsigned short specificLocationId() const;


    /**
     * @brief placement returns agent's current location
     * @pre -
     * @return weak pointer to location, or nullptr if agent is not on board
     * @post Exception guarantee: nothrow
     */
    virtual std::weak_ptr<Interface::Location> placement() const;

    /**
     * @brief setPlacement sets agent's information about its own location
     * @pre -
     * @param placement location in which agent is being sent, or nullptr if none
     * @post agent knows it's own position on board
     * @post Exception guarantee: nothrow
     */
    virtual void setPlacement(std::weak_ptr<Interface::Location> placement);

    /**
     * @brief connections tells the current level of connections
     * @pre -
     * @return returns connection level
     * @post Exception guarantee: nothrow
     * @invariant 0 <= connections <= 10
     */
    virtual unsigned short connections() const;
    /**
     * @brief setConnections sets agent's connection level
     * @pre connections are at range 0-10
     * @param connections new connection level
     * @post connections set to given argument
     * @post Exception guarantee: strong
     * @exception StateException if invariant broken
     */
    virtual void setConnections(unsigned short connections);
    /**
     * @brief modifyConnections changes agents connections by given amount
     * @pre -
     * @param amount change of connections
     * @post connections are changed the amount, if the move is not legal,
     * connections are left to closest valid value.
     * @post Exception guarantee: Strong
     * @exception StateException if invariant broken

     */
    virtual void modifyConnections(short amount);

    /**
     * @brief invariant checks whether agent's current inner state is valid.
     * This function can be called to agent at any time.
     * @pre -
     * @exception StateException is thrown when invariant breaks
     */
    virtual void invariant();

    /**
     * @brief invariant2 checks whether agent's current state is valid on board,
     * if not, it throws StateException. This is more sophisticated invariant
     * compared to 'invariant()'.
     * @pre agent is not in the middle of creation process, or in the middle
     * of change process
     * @param game shared poiner to the game, if omitted location decks aren't checked
     * @exception StateException is thrown when invariant breaks
     */
    void invariant2(std::shared_ptr<const Interface::Game> game = nullptr);




private:
    const QString name_;
    const QString title_;
    const bool common_;
    unsigned short specific_id_;
    std::weak_ptr<Interface::Location> location_;

    std::weak_ptr<Interface::Player> owner_;
    unsigned short connections_;
};

#endif // AGENT_H
