#ifndef RECOURCEGATHERINGACTION_H
#define RECOURCEGATHERINGACTION_H


#include "actioninterface.h"
#include "agent.h"
#include "location.h"
#include "player.h"

#include <memory>


/**
 * @file
 * @brief Action class for agents to gather recources on board
 */
using Interface::ActionInterface;
using Interface::Location;
using Interface::Player;
using std::shared_ptr;


/**
 * @brief The RecourceGatheringAction class implements agent's operation for
 * gathering recources for player. The recource is drawing agent and recource
 * cards from deck.
 */
class ResourceGatheringAction: public ActionInterface
{
public:
    /**
     * @brief RecourceGatheringAction constructor
     * @param shared pointer player who draws a card
     * @param shared pointer location card is drawn
     * @pre -
     * @post action created
     */
    ResourceGatheringAction(shared_ptr<Interface::Player> player, shared_ptr<Location> location);

    /**
     * @brief ~RecourceGatheringAction destructor
     */
    virtual ~ResourceGatheringAction() = default;

    /**
     * @brief canPerform checks whether it is possible to draw a card
     * @pre -
     * @return true, iff agent on board and agent capable for drawing other cards
     * @post exception gurantee: nothrow
     */
    virtual bool canPerform() const;

    /**
     * @brief perform draws a agent card to player
     * @pre canPerform() == true
     * @post action performed
     * @post exception gurantee: basic
     */
    virtual void perform();

private:
    shared_ptr<Player> player_;
    shared_ptr<Location> location_;

};

#endif // RECOURCEGATHERINGACTION_H
