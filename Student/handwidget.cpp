#include "handwidget.h"


HandWidget::HandWidget(QGraphicsScene *scene, std::shared_ptr<DragManager> dragManager, std::shared_ptr<Interface::Game> game) : QGraphicsView(scene)
{
    // setViewport(new QOpenGLWidget);
    setRenderHint(QPainter::Antialiasing, true);

    setMinimumHeight(310);
    setMaximumHeight(310);

    game_ = game;
    dragManager_ = dragManager;

    for(int i = 0; i < slotCount_; i++) {
        HandSlot *handSlot = new HandSlot(dragManager_);
        handSlot->setPos(200*i, 10);
        scene->addItem(handSlot);
        handSlots_.push_back(handSlot);
    }

    show();
}

void HandWidget::clearHand()
{
    for (HandSlot *handSlot : handSlots_) {
        Card *card = handSlot->getCard();
        handSlot->setCard(nullptr);
        if (card != nullptr) {
            scene()->removeItem(card);
            qDebug() << "Deleting card" << card;
            delete card;
        }
    }
}

void HandWidget::loadHand()
{
    clearHand();
    std::vector<std::shared_ptr<Interface::CardInterface>> cards = game_->currentPlayer()->cards();

    if (cards.size() > 5) {
        qDebug() << "Error: too many cards in hand";
    } else {
        for(unsigned int i = 0; i < cards.size(); i++) {
            qDebug() << cards[i]->name() << cards[i]->title();

            AgentCard *card = new AgentCard(dragManager_, std::static_pointer_cast<Agent>(cards[i]));
            handSlots_[i]->setCard(card);
            card->setSlot(handSlots_[i]);
            card->setPos(200*i, 10);
            scene()->addItem(card);
        }
    }
}
