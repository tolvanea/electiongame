#ifndef OWNEDINFLUENCECARD_H
#define OWNEDINFLUENCECARD_H

#include "actioninterface.h"
#include "agent.h"
#include "location.h"
#include "player.h"
#include "influence.h"
#include "cardinterface.h"


#include <memory>


/**
 * @file
 * @brief Negative influence card class
 */

using Interface::ActionInterface;
using Interface::Location;
using Interface::CardInterface;
using Interface::Player;
using Interface::Influence;
using std::shared_ptr;

/**
 * @brief The OwnedInfluenceCard class implements gathered influence cards.
 */
class OwnedInfluenceCard: public CardInterface
{
public:
    /**
     * @brief OwnedInfluenceCard constructor
     * @pre -
     * @param name of the card
     * @param player to whom this card is given
     * @param amount the amount of positive or negative influence
     * @post Card is given to player and remains to end of game
     */
    explicit OwnedInfluenceCard(const QString& name, shared_ptr<Player> player , short amount);
    /**
     * @brief OwnedInfluenceCard constructor from existing influence card
     * @pre -
     * @param influence_card existing influence card
     * @post Card is given to player and remains to end of game
     */
    explicit OwnedInfluenceCard(shared_ptr<Influence> influence_card);

    /**
     * @brief destructor
     */
    virtual ~OwnedInfluenceCard() = default;

    /**
     * @brief typeName gets cards type
     * @pre -
     * @return QString typename
     * @post exception gurantee: nothrow
     */
    virtual QString typeName() const;

    /*!
     * @brief name gets cards name
     * @pre -
     * @return Qstring name of card
     * @post exception gurantee: nothrow
     */
    QString name() const;

    /*!
     * @brief title gets cards title
     * @pre -
     * @return Qstring title of card
     * @post exception gurantee: nothrow
     */
    QString title() const;

    /**
     * @brief location gets the location, in this case it never should have one
     * @pre -
     * @return weak_ptr to location, which is in this case always nullptr
     * @post exception gurantee: nothrow
     */
    std::weak_ptr<Location> location() const;

    /**
     * @brief owner returns the owner
     * @pre -
     * @return weak_ptr to player whose card it is.
     * @post exception gurantee: nothrow
     */
    std::weak_ptr<Player> owner() const;

    /**
     * @brief setOwner sets new owner to card.
     * @pre -
     * @param owner weak_ptr to owner
     * @post card has new owner
     * @post exception gurantee: nothrow
     */
    void setOwner(std::weak_ptr<Player> owner);

    /**
     * @brief amount gets the influence card has
     * @pre -
     * @return palauttaa kortin vaikutusvallan
     * @post exception gurantee: nothrow
     */
    short amount() const;

private:
    const QString name_;
    const std::weak_ptr<Location> location_;

    std::weak_ptr<Player> owner_;
    const short amount_;
};

#endif // OWNEDINFLUENCECARD_H
