#ifndef RESOURCESABOTAGEACTION_H
#define RESOURCESABOTAGEACTION_H

#include "actioninterface.h"
#include "agent.h"
#include "location.h"
#include "player.h"


#include <memory>


/**
 * @file
 * @brief Action class for saboteurs to give other players bad influence-cards.
 */

using Interface::ActionInterface;
using Interface::Location;
using Interface::Player;
using std::shared_ptr;

/**
 * @brief The ResourceSabotageAction class implements saboteurs' action to give
 * other players unwanted influence cards. These cards affect negatively on point
 * counting at the end of game. Two cards is given randomly to agents on field.
 */
class ResourceSabotageAction: public ActionInterface
{
public:
    /**
     * @brief ResourceSabotageAction constructor
     * @param shared pointer to agent
     * @pre -
     * @post action created
     */
    ResourceSabotageAction(shared_ptr<Agent> agent, shared_ptr<Agent> victim);

    /**
     * @brief ~ResourceSabotageAction destructor
     */
    virtual ~ResourceSabotageAction() = default;

    /**
     * @brief canPerform checks whether it is possible to give bad influence cards
     * @pre -
     * @return true, iff agent is saboteur and there are other players' agents
     * on that location.
     * @post exception gurantee: nothrow
     */
    virtual bool canPerform() const;

    /**
     * @brief perform gives victim a bad influence card.
     * @pre canPerform() == true
     * @pre action performed
     * @post exception gurantee: basic
     */
    virtual void perform();

private:
    shared_ptr<Agent> agent_;
    shared_ptr<Agent> victim_;
    shared_ptr<Location> location_;
    shared_ptr<Player> owner_;
};

#endif // RESOURCESABOTAGEACTION_H
