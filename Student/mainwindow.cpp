#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->newGameButton, &QPushButton::clicked, this, &MainWindow::startGame);
    connect(ui->aboutButton, &QPushButton::clicked, this, &MainWindow::showAbout);
    setWindowTitle("ElectionGame");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startGame()
{
    hide();
    /*
    GameWindow gameWindow;
    gameWindow.exec();
    */
    StartWindow startWindow;
    startWindow.exec();
    show();
}

void MainWindow::showAbout()
{
    AboutWindow aboutWindow(this);
    aboutWindow.exec();
}
