#include "board.h"

Board::Board(QGraphicsScene *scene, std::shared_ptr<Interface::Game> game)
{
    game_ = game;
    width_ = scene->width();
    height_ = scene->height();

    Bookkeeping::B.setBoard(this);
}

QRectF Board::boundingRect() const
{
    return QRectF(0, 0, width_, height_);
}

void Board::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::darkGray);

    // Border
    painter->drawRect(0, 0, width_, height_);

    // Background
    painter->setBrush(Qt::lightGray);
    painter->drawRect(0.1*width_, 0.1*height_, 0.8*width_, 0.8*height_);

    painter->drawPixmap(0, 0, width_, height_, QPixmap(":/background.png"));

    // Center cross
    double crossThickness = 20;
    painter->setBrush(Qt::yellow);
    painter->drawRect(0.5*width_- crossThickness/2, 0, crossThickness, height_);
    painter->drawRect(0, 0.5*height_ - crossThickness/2, width_, crossThickness);

    /*
    // Label backgrounds
    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::white);
    painter->drawRect(QRect(0.2*width_-10, 100, 600, 100));
    painter->drawRect(QRect(0.7*width_-10, 100, 600, 100));
    painter->drawRect(QRect(0.2*width_-10, 0.95*height_, 600, 100));
    painter->drawRect(QRect(0.7*width_-10, 0.95*height_, 600, 100));
    */

    // Area labels
    painter->setPen(Qt::black);
    painter->setFont(labelFont_);
    painter->drawText(0.11*width_, 910, game_->locations()[0]->name());
    painter->drawText(0.7*width_, 910, game_->locations()[1]->name());
    painter->drawText(0.7*width_, 1060, game_->locations()[2]->name());
    painter->drawText(0.11*width_, 1060, game_->locations()[3]->name());

    // Influence readings
    painter->drawText(0.11*width_, 960, QString("Influence: ") + QString::number(game_->locations()[0]->influence(game_->currentPlayer())));
    painter->drawText(0.7*width_, 960, QString("Influence: ") + QString::number(game_->locations()[1]->influence(game_->currentPlayer())));
    painter->drawText(0.7*width_, 1110, QString("Influence: ") + QString::number(game_->locations()[2]->influence(game_->currentPlayer())));
    painter->drawText(0.11*width_, 1110, QString("Influence: ") + QString::number(game_->locations()[3]->influence(game_->currentPlayer())));
}
