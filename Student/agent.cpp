#include "agent.h"

#include "location.h"
#include "deckinterface.h"
#include "cardinterface.h"
#include "player.h"
#include "stateexception.h"


#include <memory>
#include <vector>
#include <QDebug>

using std::shared_ptr;
using Interface::Location;
using Interface::DeckInterface;
using Interface::CardInterface;
using Interface::StateException;



Agent::Agent(const QString &name, const QString &title, bool common, unsigned short id):
    name_(name),
    title_(title),
    common_(common),
    specific_id_(id),
    location_(),
    owner_(),
    connections_(1)
{

}

QString Agent::typeName() const
{
    return "Agent";
}

QString Agent::name() const
{
    return name_;
}

QString Agent::title() const
{
    return title_;
}

std::weak_ptr<Interface::Location> Agent::location() const
{
    return location_;
}

std::weak_ptr<Interface::Player> Agent::owner() const
{
    return owner_;
}

void Agent::setOwner(std::weak_ptr<Interface::Player> owner)
{
    owner_ = owner;
}

bool Agent::isCommon() const
{
    return common_;
}

unsigned short Agent::specificLocationId() const
{
    return specific_id_;
}

std::weak_ptr<Interface::Location> Agent::placement() const
{
    return location_;
}

void Agent::setPlacement(std::weak_ptr<Interface::Location> placement)
{
    location_ = placement;
}

unsigned short Agent::connections() const
{
    return connections_;
}

void Agent::setConnections(unsigned short connections)
{
    invariant();
    unsigned short old_connections = connections_;
    try{
        connections_ = connections;
        invariant();
    }
    catch (Interface::StateException e){
        connections_ = old_connections;
        throw e;
    }
}

void Agent::modifyConnections(short amount)
{
    invariant();
    unsigned short old_connections = connections_;

    try{
        // unsigned int never gets negative, it jumps to highest value below 0
        if (connections_ < -amount){
            throw Interface::StateException(QString("Connections too low"));
        }
        else{
            connections_ += amount;
        }
        invariant();
    }
    catch (Interface::StateException e){
        connections_ = old_connections;
        throw e;
    }
}

void Agent::invariant()
{
    if (10 < connections_){
        throw Interface::StateException(QString("Connections too high"));
    }
    else if (connections_ <= 0){
        throw Interface::StateException(QString("Connections too low"));
    }
}

void Agent::invariant2(shared_ptr<const Interface::Game> game)
{
    // if game object was not passed
    if (game==nullptr){

        //just as reminder of c++ boolean logic:
        //      logical implication (->)  !p || q
        //      exclusive or        (xor)  p != q
        //      if and only if      (iff)  p == q

        // if agent is on board, then connections are at least 1, and at most 10
        if (not (!(location().lock()!=nullptr) || (1<=connections_ && connections_<=10))){
            throw Interface::StateException(QString("Agent has invalid number of connections to be on board"));
        }

        if (owner_.lock() != nullptr){
            // finding out if *this agent is in hand by using std::find_if() and
            // lambda function
            std::vector<shared_ptr<CardInterface>> hand_vec = owner_.lock()->cards();
            bool is_in_hand =
                    (std::find_if(hand_vec.begin(),
                                  hand_vec.end(),
                                  [this](shared_ptr<CardInterface> agent_in_hand)->bool{
                                      return (agent_in_hand.get() == this);
                                  })
                    ) != hand_vec.end();

            // if agent has an owner, then agent is in hand xor in location
            if (not (!(owner_.lock()!=nullptr) || (is_in_hand != (location_.lock() != nullptr)))){
                qDebug()<<"Ivariant fail: 'if agent has an owner, then agent is in hand xor in location'. "<<"(is_in_hand) XOR (location_.lock() != nullptr)"<<is_in_hand<<"!="<<(location_.lock() != nullptr);
                throw Interface::StateException("Agent is not in hand nor in location");
            }

            // agent can't be both in hand and in location at the same time
            if (not (is_in_hand == (location_.lock() == nullptr))){
                throw Interface::StateException("Agent is in hand and location at the same time");
            }
        }

        // if agent is non-generic, it has location-id 0-3 (specific_id_ is unsigned)
        if (not (!isCommon() || ( specific_id_<=3))){
            throw Interface::StateException("Agent has invalid specific location-id");
        }
    }

    // if game-object was passed, location decks can be iterated
    else{
        // find out if agent is in any deck
        int occurrences_in_decks = 0;
        shared_ptr<const Location> found_location;
        // iterate locations
        for (shared_ptr<const Location> location : game->locations()){
            shared_ptr<const DeckInterface> deck = location->deck();
            // iterate deck
            for(unsigned int i = 0; i < deck->size(); ++i){
                if (deck->cardAt(i).get() == this){
                    ++occurrences_in_decks;
                    found_location = location;
                }
            }
        }

        // agent can't be in multible decks
        if (not ((occurrences_in_decks == 0) || (occurrences_in_decks == 1))){
            throw Interface::StateException("Agent found on multible locations at the same time");
        }

        // agent has no owner iff it is in some deck
        if (not ((owner_.lock() == nullptr) == (occurrences_in_decks == 1))){
            throw Interface::StateException("Agent at deck found to have an owner");
        }

        // member variable 'location_' is the same as board's data about location
        if (occurrences_in_decks == 1)
            if (not (location_.lock() == found_location)){
                throw Interface::StateException("Mismatch between agent's and game's location information.");
            }

    }

}

