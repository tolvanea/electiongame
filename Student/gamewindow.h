#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include "boardwidget.h"
#include "endwindow.h"
#include "handwidget.h"
#include "statswidget.h"

#include "game.h"
#include "runner.h"
#include "actioninterface.h"
#include "controlinterface.h"
#include "manualcontrol.h"

#include <QDialog>
#include <QGraphicsView>


class StatsWidget;

namespace Ui {
class GameWindow;
}

class GameWindow : public QDialog
{
    Q_OBJECT

public:
    explicit GameWindow(QWidget *parent = 0);
    ~GameWindow();

public slots:

    /**
     * @brief actionPerformed This is part of the logic of the game. Every time
     * some action-button is pressed, this function perform necessary processing
     * for the player move.
     * @param action shared pointer to action
     */
    void actionPerformed(std::shared_ptr<Interface::ActionInterface> action, bool no_turn_count);

    /**
     * @brief changeTurn Changes the turn for next player, checks also if game ends
     */
    void changeTurn();

protected:
    /**
     * @brief keyPressEvent overrides the escape key from closing the window
     * @param event
     */
    void keyPressEvent(QKeyEvent *event) override;

private:
    // Ui::GameWindow *ui;

    /**
     * @brief createEngine Initializes game, locations, players and cards.
     */
    void createEngine();

    /**
     * @brief createWidgets creates the widgets of the window
     */
    void createWidgets();

    /**
     * @brief connectSingnals creates the signal-slot connections to ValidateActions
     */
    void connectSingnals();

    /**
     * @brief startGame starts the game and sets up everything for the first round.
     */
    void startGame();

    /**
     * @brief endGame ends the game and shows the winner.
     */
    void endGame();

    /**
     * @brief getPointer returns pointer to GameWindow object.
     * @return
     */
    GameWindow* getPointer();

    std::shared_ptr<Interface::Game> game_;
    std::shared_ptr<Interface::Runner> runner_;

    std::shared_ptr<DragManager> dragManager_;

    QPushButton *changeTurnButton_;

    HandWidget *handWidget_;
    BoardWidget *boardWidget_;
    StatsWidget *statsWidget_;

    QGraphicsScene *boardScene_;
    QGraphicsScene *handScene_;

    std::shared_ptr<Interface::ControlInterface> current_players_control_;


};

#endif // GAMEWINDOW_H
