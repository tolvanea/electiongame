#include "connectionboostaction.h"

#include "cardinterface.h"
#include "bookkeeping.h"
#include "statswidget.h"

#include <QDebug>


using Interface::CardInterface;


ConnectionBoostAction::ConnectionBoostAction(shared_ptr<Agent> agent, shared_ptr<Agent> receiver_agent):
    agent_(agent),
    receiver_(receiver_agent),
    location_(agent_->placement().lock()),
    owner_(agent_->owner().lock())
{

}

bool ConnectionBoostAction::canPerform() const
{
    // it's smarter to upgrade the last step with connection raising action
    bool connections_to_raise = (receiver_->connections()<9);

    bool agent_has_owner = (owner_ != nullptr);

    bool own_agent = (owner_ == receiver_->owner().lock());

    std::vector<shared_ptr<CardInterface>> hand_vec = owner_->cards();
    bool is_in_hand = (std::find(hand_vec.begin(), hand_vec.end(), agent_) != hand_vec.end());

    return connections_to_raise && agent_has_owner && own_agent && is_in_hand;
}

void ConnectionBoostAction::perform()
{
    bool db =  true; //debug messages
    agent_->invariant2(Bookkeeping::B.getGame().lock());
    receiver_->invariant2(Bookkeeping::B.getGame().lock());
    unsigned short old_connections = receiver_->connections();

    if (10 < receiver_->connections()+5){
        receiver_->setConnections(10);
    }
    else
        receiver_->modifyConnections(5);

    agent_->owner().lock()->playCard(agent_);

    if (db) qDebug()<<"        ConnectionBoostAction"<<old_connections<<"+ 5 ="<<receiver_->connections();
    Bookkeeping::B.getStatsWidget()->showText(0, "Connections boosted: ");
    Bookkeeping::B.getStatsWidget()->showText(1, QString::number(old_connections) +
                                              " -> " +
                                              QString::number(receiver_->connections()));

    receiver_->invariant2(Bookkeeping::B.getGame().lock());

}
