#ifndef STARTWINDOW_H
#define STARTWINDOW_H

#include "bookkeeping.h"
#include "gamewindow.h"

#include <QDialog>

#include <vector>

namespace Ui {
class StartWindow;
}

/**
 * @brief The StartWindow class is a window for asking the player names and colours
 */
class StartWindow : public QDialog
{
    Q_OBJECT

public:
    explicit StartWindow(QWidget *parent = 0);
    ~StartWindow();

public slots:
    /**
     * @brief startGame checks if there are enough players and if so, it sends
     * the names and colours to Bookkeeping and starts GameWindow
     */
    void startGame();

    /**
     * @brief setColor asks the player for their colour and sets it to their color button
     * @param playerNumber the index of the player (0-3)
     * @post Exception guarantee: basic
     */
    void setColor(int playerNumber);

private:
    Ui::StartWindow *ui;

    std::vector<QToolButton*> colorButtons_;
    std::vector<QLineEdit*> nameEdits_;
};

#endif // STARTWINDOW_H
