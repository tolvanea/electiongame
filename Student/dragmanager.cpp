#include "dragmanager.h"

DragManager::DragManager()
{

}

Card *DragManager::getCard() const
{
    invariant();

    if (draggedCard_ != nullptr && dragging_)
        return draggedCard_;
    else
        throw Interface::StateException("There is no dragging at the moment");
}

void DragManager::startDrag(Card *card, bool valid_grab)
{
    invariant();

    if (dragging_){
        throw Interface::StateException("Only one drag is allowed at a time");
    } else {
        draggedCard_ = card;
        dragging_ = true;
        valid_grab_ = valid_grab;
    }

    invariant();
}

void DragManager::endDrag()
{
    invariant();

    /*
    if(dragging_) {
        qDebug() << "Ending a drag that did not exist";
    }
    */
    dragging_ = false;
    draggedCard_ = nullptr;
    valid_grab_ = false;

    invariant();
}

bool DragManager::getDragging() const
{
    return dragging_;
}

bool DragManager::getValidGrab() const
{
    return valid_grab_;
}

void DragManager::invariant() const
{
    // Q_ASSERT( ( !(draggedCard_ == nullptr) ) == dragging_ );
    if ( (!(draggedCard_ == nullptr) != dragging_)) {
        throw Interface::StateException("DragManager invariant failed");
    }
}
