#include "influencegatheringaction.h"

#include "actioninterface.h"
#include "agent.h"
#include "player.h"
#include "bookkeeping.h"
#include "statswidget.h"

#include <string>
#include <QDebug>

using Interface::Player;
using std::shared_ptr;
using std::weak_ptr;


InfluenceGatheringAction::InfluenceGatheringAction(std::shared_ptr<Agent> agent):
    agent_(agent)
{

}

bool InfluenceGatheringAction::canPerform() const
{
    bool is_on_board = (agent_->placement().lock() != nullptr);

    bool is_correct_type = (agent_->title() == "Subordinate") ||
            (agent_->title() == "Volunteer");

    return is_on_board && is_correct_type;
}

void InfluenceGatheringAction::perform()
{
    bool db =  true; //debug messages
    agent_->invariant2();

    unsigned short influence = agent_->placement().lock()->influence(agent_->owner().lock());
    unsigned short old_influence = influence;

    // TO-DECIDE how exactly influence raises, for example how much do
    // agents connections matter?
    if (influence<100)
        influence += 100 * (1 + agent_->connections());
    else if ((5000 < influence) && (influence <= 9000)){
        influence += 1000;
    }
    else if (9000 < influence){
        // its OVER NINE THOUSAND!
        // we gotta have roof somewhere to maintain even some sense in this
        influence = 10000;
    }
    else {
        // main case
        influence *= (1 + 0.05*agent_->connections());
    }

    agent_->placement().lock()->setInfluence(agent_->owner().lock(), influence);
    if (db) qDebug()<<"        influenceGatheringAction: "<<old_influence<<"+"<<(influence-old_influence)<<"="<<influence;


    // check locational unlocks
    {
        weak_ptr<const Player> owner = agent_->owner();
        weak_ptr<const Location> location = agent_->location();

        if (1000 <= influence && Bookkeeping::B.isNextLocked(owner,location)){
            Bookkeeping::B.unlockNext(owner, location);
            if (db) qDebug()<<"        influenceGatheringAction: Next stage unlocked!";
            Bookkeeping::B.getStatsWidget()->showText(0, "Influence: "+QString::number(influence)+"/1000");
            Bookkeeping::B.getStatsWidget()->showText(1, "next stage unlocked!");

            if (5000 <= influence){
                Bookkeeping::B.getStatsWidget()->showText(0, "Influence is over 5000");
                Bookkeeping::B.getStatsWidget()->showText(1, "slowing growth "+
                                                          QString::number(old_influence)+
                                                          " -> "+QString::number(influence));
            }
            else if (10000 <= influence){
                Bookkeeping::B.getStatsWidget()->showText(0, "Influence is 10000");
                Bookkeeping::B.getStatsWidget()->showText(1, "Go to somewhere else");
            }
        }

        else{
            Bookkeeping::B.getStatsWidget()->showText(0, "Influence gathered on ");
            Bookkeeping::B.getStatsWidget()->showText(1, location.lock()->name() +": " + QString::number(old_influence)+" -> "+QString::number(influence));

            if (agent_->connections() < 4){
                Bookkeeping::B.getStatsWidget()->showText(0, "Influence: " +
                                                          QString::number(old_influence)+
                                                          " -> "+
                                                          QString::number(influence));
                Bookkeeping::B.getStatsWidget()->showText(1, "pro-tip: Get more connections");
            }
        }

    }

    agent_->invariant2();
}
