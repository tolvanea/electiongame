#include "aboutwindow.h"
#include "ui_aboutwindow.h"

AboutWindow::AboutWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutWindow)
{
    setWindowModality(Qt::WindowModal);
    ui->setupUi(this);
}

AboutWindow::~AboutWindow()
{
    delete ui;
}
