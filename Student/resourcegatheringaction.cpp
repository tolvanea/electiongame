#include "resourcegatheringaction.h"

#include "actioninterface.h"
//#include "agent.h"
#include "location.h"
#include "player.h"
#include "deckinterface.h"
#include "cardinterface.h"
#include "influence.h"
#include "ownedinfluencecard.h"
#include "bookkeeping.h"
#include "statswidget.h"
#include "gameexception.h"

#include <memory>
#include <QDebug>



using Interface::Location;
using Interface::Player;
using Interface::DeckInterface;
using Interface::CardInterface;
using Interface::Influence;
using std::shared_ptr;


ResourceGatheringAction::ResourceGatheringAction(shared_ptr<Player> player, shared_ptr<Location> location):
    player_(player),
    location_(location)
{

}

bool ResourceGatheringAction::canPerform() const
{
    bool location_unlocked =
        (Bookkeeping::B.isUnlocked(player_,location_));

    bool has_correct_agent_on_board = false;
    for (auto agent : location_->agents()){
        if ((agent->owner().lock() == player_) && (agent->title() == "Subordinate")){
            has_correct_agent_on_board = true;
        }
    }

    if (! has_correct_agent_on_board){
        Bookkeeping::B.getStatsWidget()->showText(0, "No subordinate on ");
        Bookkeeping::B.getStatsWidget()->showText(1, location_->name());
    }

    bool deck_has_cards = location_->deck()->canDraw();

    return location_unlocked && has_correct_agent_on_board && deck_has_cards;
}

void ResourceGatheringAction::perform()
{
    bool db =  true; //debug messages
    shared_ptr<DeckInterface> deck = location_->deck();

    shared_ptr<CardInterface> card = deck->draw();

    if (card->typeName() == "Influence"){
        shared_ptr<Influence> influence_card = std::dynamic_pointer_cast<Influence>(card);
        shared_ptr<OwnedInfluenceCard> owned_influence_card =
                std::make_shared<OwnedInfluenceCard>(influence_card);

        Bookkeeping::B.giveInfluenceCard(player_, owned_influence_card);
        Bookkeeping::B.getStatsWidget()->showText(0, "New influence card acquired:");
        Bookkeeping::B.getStatsWidget()->showText(1, card->name());
    }
    else if (card->typeName() == "Agent"){
        player_->addCard(card);
    }
    else{
        throw Interface::GameException("Unkown type of card in deck");
    }
    if (db) qDebug()<<"        ResourceGatheringAction";
}
