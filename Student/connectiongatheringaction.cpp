#include "connectiongatheringaction.h"

#include "actioninterface.h"
#include "agent.h"
#include "player.h"
#include "bookkeeping.h"
#include "statswidget.h"

#include <QDebug>


ConnectionGatheringAction::ConnectionGatheringAction(std::shared_ptr<Agent> agent):
    agent_(agent)
{

}

bool ConnectionGatheringAction::canPerform() const
{
    bool is_on_board = (agent_->placement().lock() != nullptr);

    return is_on_board;

}

void ConnectionGatheringAction::perform()
{
    bool db =  true; //debug messages
    agent_->invariant2(Bookkeeping::B.getGame().lock());

    unsigned short old_connections = agent_->connections();

    // add one level to connections
    if (10 <= old_connections){
        agent_->setConnections(10);
    }
    else{
        agent_->modifyConnections(1);
    }

    if (db) qDebug()<<"        ConnectionGatheringAction "<<old_connections<<"+ 1 ="<<agent_->connections();
    Bookkeeping::B.getStatsWidget()->showText(0, "Connections gathered: ");
    Bookkeeping::B.getStatsWidget()->showText(1, QString::number(old_connections) +
                                              " -> " +
                                              QString::number(agent_->connections()));

    agent_->invariant2(Bookkeeping::B.getGame().lock());


}
