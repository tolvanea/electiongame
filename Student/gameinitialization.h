#ifndef GAMEINITIALIZATION_H
#define GAMEINITIALIZATION_H

#include "game.h"
#include "boardwidget.h"
#include "handwidget.h"
#include "influence.h"
#include "location.h"
#include "deckinterface.h"

#include "agent.h"
#include "bookkeeping.h"

#include <memory>
#include <vector>
#include <map>

using Interface::Game;
using Interface::Influence;
using Interface::Location;
using Interface::Player;

using std::make_shared;
using std::shared_ptr;


/**
 * @file
 * @brief This module is for functions that are called at the game initialization. Initializes the cards.
 * process.
 */



using Interface::Game;
using Interface::Influence;
using Interface::Location;
using Interface::Player;

namespace Initialization {

/**
 * @brief create_influence_cards creates influence cards and adds them to the
 * correspondig decks.
 * @param game shared pointer to game
 */
void createInfluenceCards(shared_ptr<Game> game);

/**
 * @brief create_saboteurs_agents creates sabouteurs and adds them to location
 * decks
 * @param game shared pointer to game
 */
void createSaboteursAgents(shared_ptr<Game> game);

/**
 * @brief create_supporters creates supporters, general agents for gatherin recources
 * @param game
 */
void createSupporters(shared_ptr<Game> game);

/**
 * @brief create_special_agents creates general special agent cards
 * @param game shared pointer to game
 */
void createSpecialAgents(shared_ptr<Game> game);

/**
 * @brief create_general_agents gives every player two general "subordinate"-agents
 * @param player shared pointer to player
 * @param player_name owner's name
 */
void createGeneralAgents(shared_ptr<Player> player, QString player_name);

/**
 * @brief read_settings reads settings from external file. Uses defaults when
 * reading fails, or no data is present.
 * @return map containin QString keys and int values for settings
 */
std::map<QString, int> readSettings();

} // Initialization


#endif // GAMEINITIALIZATION_H
