#include "sendaction.h"

#include "actioninterface.h"
#include "agent.h"
#include "player.h"

#include "bookkeeping.h"
#include "statswidget.h"

#include <string>
#include <QDebug>

using Interface::Location;

SendAction::SendAction(std::shared_ptr<Agent> agent,
                       std::shared_ptr<Interface::Location> location):
    agent_(agent), location_(location)
{

}


bool SendAction::canPerform() const
{
    bool db = false; //debug messages

    // This will check the following:
    //      agent is correct type to been send to that location
    //      location is not locked for that player

    bool is_correct_type;
    if (agent_->title() == "Services"){
        is_correct_type = false;
    }
    else if (agent_->isCommon()){
        is_correct_type = true;
    }
    else{
        is_correct_type = (agent_->specificLocationId() == location_->id());
    }

    bool is_unlocked = Bookkeeping::B.isUnlocked(agent_->owner(), location_);

    // print help info
    if (!is_unlocked){
        if (agent_->location().lock() != nullptr){
            Bookkeeping::B.getStatsWidget()->showText(0, location_->name() + " is locked ");
            Bookkeeping::B.getStatsWidget()->showText(1, "Influece: " +
                        QString::number(agent_->location().lock()->
                                        influence(Bookkeeping::B.getGame().lock()->
                                                  currentPlayer())
                                        ) + "/1000");
        }

    }


    if (db) qDebug()<<"        SendAction: is_correct_type<<is_unlocked"
                   <<is_correct_type<<is_unlocked;

    return is_correct_type && is_unlocked;

}

void SendAction::perform()
{
    bool db =  true; //debug messages

    // agent is sent from player's hand
    if (agent_->location().lock() == nullptr){
        agent_->owner().lock()->playCard(agent_);
        // adds card to location AND updates agents information about its location
        location_->sendAgent(agent_);

        agent_->setConnections(1);
        if (db) qDebug()<<"        SendAction: "<<"agent is sent from player's hand";
    }

    // agent is sent from other location
    else{
        if (db) qDebug()<<"        SendAction: "<<agent_->placement().lock()->name()<<" -> "<<location_->name();

        //resets agent's connections on location change
        if (agent_->placement().lock() != location_){
            agent_->setConnections(1);
            if (agent_->connections() != 1){
                Bookkeeping::B.getStatsWidget()->showText(0, agent_ ->name()+
                                                          " lost connections on");
                Bookkeeping::B.getStatsWidget()->showText(1, "location change");
            }
        }

        agent_->placement().lock()->removeAgent(agent_);
        location_->sendAgent(agent_);

    }

}
