#ifndef HANDWIDGET_H
#define HANDWIDGET_H

#include "card.h"
#include "cardslot.h"
#include "dragmanager.h"

#include "game.h"
#include "player.h"

#include <QtWidgets>

#include <memory>
#include <vector>

/**
 * @brief The HandWidget class is for displaying the player's hand and enabling card moves to and from it
 */
class HandWidget : public QGraphicsView
{
    Q_OBJECT

public:
    explicit HandWidget(QGraphicsScene *scene, std::shared_ptr<DragManager> dragManager, std::shared_ptr<Interface::Game> game);

public slots:
    /**
     * @brief clearHand clears the UI hand card slots
     * @post Exception guarantee: minimal
     */
    void clearHand();

    /**
     * @brief loadHand sets the visible player hand to the one in the game logic
     * @pre the game is in such a state that its currentPlayer() works as intended
     * @post Exception guarantee: minimal
     */
    void loadHand();

private:
    std::shared_ptr<Interface::Game> game_;
    std::shared_ptr<DragManager> dragManager_;
    const int slotCount_ = 5;
    std::vector<HandSlot*> handSlots_;
};

#endif // HANDWIDGET_H
