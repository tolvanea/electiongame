#ifndef STATSWIDGET_H
#define STATSWIDGET_H

#include "game.h"

#include "bookkeeping.h"
#include "card.h"

#include <QtWidgets>

#include <vector>
#include <memory>

/**
 * @brief The StatsWidget class describes a widget that shows game statistics
 */
class StatsWidget : public QWidget
{
public:
    StatsWidget(QWidget *parent, std::shared_ptr<Interface::Game> game);
    ~StatsWidget();

public slots:
    /**
     * @brief updateStats should be called when the stats need to be redrawn
     */
    void updateStats();

    /**
     * @brief showText Shows text in the third box
     * @param row the row to draw on (0 or 1)
     * @param text The text to draw
     * @pre row == 0 or row == 1
     */
    void showText(const unsigned short &row, const QString &text);

private:
    /**
     * @brief setupPrimary configures the primary statistics display
     * @post Exception guarantee: minimal
     */
    void setupPrimary();

    /**
     * @brief setupInfluence configures the influence card display
     * @post Exception guarantee: minimal
     */
    void setupInfluence();

    /**
     * @brief setupText configures the text display
     * @post Exception guarantee: minimal
     */
    void setupText();

    /**
     * @brief clearWidgets clears a QLayout from its child widgets
     * @param layout the layout to be cleared
     * @post Exception guarantee: minimal
     */
    void clearWidgets(QLayout * layout);

    std::shared_ptr<Interface::Game> game_;
    int turnsLeft_ = -9000;

    int playerCount_;
    std::vector<QLabel*> cardCountLabels_;
    std::vector<QLabel*> influenceLabels_;

    QLabel *currentPlayerLabel_;
    QLabel *actionsLeftLabel_;
    QLabel *weeksLeftLabel_;
    QVBoxLayout *layout_;
    QGroupBox *influenceBox_;
    QGridLayout *influenceLayout_;
    QLabel *textLabel0_;
    QLabel *textLabel1_;
};

#endif // STATSWIDGET_H
