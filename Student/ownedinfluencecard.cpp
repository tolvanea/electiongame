#include "ownedinfluencecard.h"


OwnedInfluenceCard::OwnedInfluenceCard(const QString &name, shared_ptr<Interface::Player> player, short amount):
    name_(name),
    location_(),
    owner_(player),
    amount_(amount)
{

}

OwnedInfluenceCard::OwnedInfluenceCard(shared_ptr<Interface::Influence> influence_card):
    name_(influence_card->name()),
    location_(),
    owner_(influence_card->owner()),
    amount_(influence_card->amount())
{

}

QString OwnedInfluenceCard::typeName() const
{
    return "Owned Influence Card";

}

QString OwnedInfluenceCard::name() const
{
    return name_;
}

QString OwnedInfluenceCard::title() const
{
    return "";
}

std::weak_ptr<Interface::Location> OwnedInfluenceCard::location() const
{
    return location_;
}

std::weak_ptr<Interface::Player> OwnedInfluenceCard::owner() const
{
    return owner_;
}

void OwnedInfluenceCard::setOwner(std::weak_ptr<Interface::Player> owner)
{
    owner_ = owner;
}

short OwnedInfluenceCard::amount() const
{
    return amount_;
}
