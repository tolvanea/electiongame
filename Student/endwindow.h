#ifndef ENDWINDOW_H
#define ENDWINDOW_H

#include "bookkeeping.h"
#include "game.h"
#include "gameexception.h"
#include "location.h"

#include <QtWidgets>

#include <limits>

namespace Ui {
class EndWindow;
}

/**
 * @brief The EndWindow class is a window for displaying the game results to players
 */
class EndWindow : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief EndWindow constructs the window and fetches results from Bookkeeping
     * @param parent parent widget (GameWindow for example)
     */
    explicit EndWindow(QWidget *parent = 0);
    ~EndWindow();

private:
    Ui::EndWindow *ui;
};

#endif // ENDWINDOW_H
