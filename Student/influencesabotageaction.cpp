#include "influencesabotageaction.h"


#include "actioninterface.h"
#include "agent.h"
#include "player.h"
#include "location.h"

#include "bookkeeping.h"
#include "statswidget.h"

#include <QDebug>


InfluenceSabotageAction::InfluenceSabotageAction(std::shared_ptr<Agent> agent):
    agent_(agent),
    location_(agent_->placement().lock()),
    owner_(agent_->owner().lock())
{

}

bool InfluenceSabotageAction::canPerform() const
{
    bool db = false; //debug messages

    bool is_on_board = (location_ != nullptr);
    if (!is_on_board) return false;

    bool other_player_on_location = false;
    for (auto agent : location_->agents()){
        if (agent->owner().lock() != owner_){
            other_player_on_location = true;
            break;
        }
    }
    if (db) qDebug()<<"is_on_board<<other_player_on_location:"<<
                      is_on_board<<other_player_on_location;
    if (!other_player_on_location){
        Bookkeeping::B.getStatsWidget()->showText(0, "No other palyers on location");
        Bookkeeping::B.getStatsWidget()->showText(1, location_->name());
        agent_->invariant2();
    }
    return other_player_on_location;
}

void InfluenceSabotageAction::perform()
{
    bool db =  true; //debug messages
    agent_->invariant2();

    for (auto agent : location_->agents()){
        if (agent->owner().lock() != owner_){
            unsigned short influence = location_->influence(agent->owner().lock());
            unsigned short old_influence = influence;
            influence *= (1-0.05*agent_->connections());
            location_->setInfluence(agent->owner().lock(),influence);
            if (db) qDebug()<<"        InfluenceSabotageAction"
                           <<agent->owner().lock()->name()<<old_influence<<"-"
                           <<old_influence-influence<<"="<<influence;
        }
    }
    Bookkeeping::B.getStatsWidget()->showText(0, "Decreased other players'");
    Bookkeeping::B.getStatsWidget()->showText(1, "influence on "+ location_->name());
    agent_->invariant2();


}
