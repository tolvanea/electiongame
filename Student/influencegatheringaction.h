#ifndef INFLUENCEGATHERINGACTION_H
#define INFLUENCEGATHERINGACTION_H

#include "actioninterface.h"
#include "agent.h"
#include "location.h"

#include <memory>


/**
 * @file
 * @brief Action class for raising players influence on specific location.
 */
using Interface::ActionInterface;
using Interface::Location;


/**
 * @brief The influenceGatheringAction class implements agent's operation for
 * gathering influence for player. The influence is for exapmle in social media
 * the number of followers.
 */
class InfluenceGatheringAction: public ActionInterface
{
public:
    /**
     * @brief influenceGatheringAction constructor
     * @param shared pointer to agent
     * @pre -
     * @post action created
     */
    InfluenceGatheringAction(std::shared_ptr<Agent> agent);

    /**
     * @brief ~influenceGatheringAction destructor
     */
    virtual ~InfluenceGatheringAction() = default;

    /**
     * @brief canPerform checks whether it is possible agent to gather influence
     * @pre -
     * @return true, iff agent on board and agent capable for gathering influence
     * @post exception gurantee: nothrow
     */
    virtual bool canPerform() const;

    /**
     * @brief perform raises players influence for that location, takes in account
     * agent's current connections.
     * @pre canPerform() == true
     * @post action performed
     * @post exception gurantee: basic
     */
    virtual void perform();

private:
    std::shared_ptr<Agent> agent_;

};

#endif // INFLUENCEGATHERINGACTION_H
