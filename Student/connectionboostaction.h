#ifndef CONNECTIONBOOSTACTION_H
#define CONNECTIONBOOSTACTION_H


#include "actioninterface.h"
#include "agent.h"
#include "location.h"
#include "player.h"


#include <memory>


/**
 * @file
 * @brief Action class for consultants to give connections
 */

using Interface::ActionInterface;
using Interface::Location;
using Interface::Player;
using std::shared_ptr;

/**
 * @brief The ConnectionBoostAction class is consultant's action to raise quickly
 * connections of agent.
 */
class ConnectionBoostAction: public ActionInterface
{
public:
    /**
     * @brief ConnectionBoostAction constructor
     * @param shared pointer to agent
     * @pre -
     * @post action created
     */
    ConnectionBoostAction(shared_ptr<Agent> agent, shared_ptr<Agent> receiver_agent);

    /**
     * @brief ~ConnectionBoostAction destructor
     */
    virtual ~ConnectionBoostAction() = default;

    /**
     * @brief canPerform checks whether it is possible to give that agent connection
     * boost
     * @pre -
     * @return true, iff agent has connections to raise, and that's own agent
     * @post exception gurantee: nothrow
     */
    virtual bool canPerform() const;

    /**
     * @brief perform gives receiver agent connection boost.
     * @pre canPerform() == true
     * @post corresponding moves are performed
     * @post exception gurantee: basic
     */
    virtual void perform();

private:
    shared_ptr<Agent> agent_;
    shared_ptr<Agent> receiver_;
    shared_ptr<Location> location_;
    shared_ptr<Player> owner_;
};


#endif // CONNECTIONBOOSTACTION_H
