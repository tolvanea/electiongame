#ifndef SENDACTION_HH
#define SENDACTION_HH

#include "actioninterface.h"
#include "agent.h"
#include "location.h"


#include <memory>

/**
 * @file
 * @brief Action class for sending agent from players hand to one location
 */
using Interface::ActionInterface;
using Interface::Location;


/**
 * @brief The SendAction class implements agent's sending from hand to board.
 */
class SendAction: public ActionInterface
{
public:

    /**
     * @brief SendAction constructor
     * @param shared pointer to agent-card
     * @pre -
     * @post send action created
     */
    explicit SendAction(std::shared_ptr<Agent> agent,
                        std::shared_ptr<Location> location);

    /**
     * @brief ~SendAction destructor
     */
    virtual ~SendAction() = default;

    /**
     * @brief canPerform checks whether it is possible to send agent to that location
     * @pre -
     * @return true, iff agent is correct type, location has free space and it is unlocked
     * @post exception gurantee: nothrow
     */
    virtual bool canPerform() const;

    /**
     * @brief perform sends agent to that location
     * @pre canPerform() == true
     * @post action performed
     * @post exception gurantee: basic
     */
    virtual void perform();

private:
    std::shared_ptr<Agent> agent_;
    std::shared_ptr<Location> location_;

};

#endif // SENDACTION_HH
