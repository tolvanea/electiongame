#include "startwindow.h"
#include "ui_startwindow.h"

StartWindow::StartWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StartWindow)
{
    Bookkeeping::B.clearAll();

    ui->setupUi(this);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &StartWindow::startGame);
    // connect(this, &QDialog::accepted, this, &StartWindow::startGame);

    connect(ui->ColorButtonP1, &QToolButton::clicked, this, [this]{ setColor(0); });
    connect(ui->ColorButtonP2, &QToolButton::clicked, this, [this]{ setColor(1); });
    connect(ui->ColorButtonP3, &QToolButton::clicked, this, [this]{ setColor(2); });
    connect(ui->ColorButtonP4, &QToolButton::clicked, this, [this]{ setColor(3); });

    colorButtons_.push_back(ui->ColorButtonP1);
    colorButtons_.push_back(ui->ColorButtonP2);
    colorButtons_.push_back(ui->ColorButtonP3);
    colorButtons_.push_back(ui->ColorButtonP4);

    const QColor initialColors[4] = { Qt::blue, Qt::red, Qt::green, Qt::yellow };

    for(unsigned int i = 0; i < 4; i++) {
        QPalette pal = colorButtons_[i]->palette();
        pal.setColor(QPalette::Button, initialColors[i]);
        colorButtons_[i]->setPalette(pal);
    }

    nameEdits_.push_back(ui->NameEditP1);
    nameEdits_.push_back(ui->NameEditP2);
    nameEdits_.push_back(ui->NameEditP3);
    nameEdits_.push_back(ui->NameEditP4);
}

StartWindow::~StartWindow()
{
    delete ui;
}

void StartWindow::startGame()
{
    unsigned short players = 0;
    std::vector<QColor> playerColors;
    for(unsigned short i = 0; i < 4; i++) {
        QString playerName = nameEdits_[i]->text();

        if(! playerName.isEmpty()) {
            Bookkeeping::B.playerNames()->push_back(playerName);

            QPalette pal = colorButtons_[i]->palette();
            QColor color = pal.color(QPalette::Button);
            Bookkeeping::B.playerColors()->push_back(color);
            players++;
        }
    }

    if (players >= 2) {
        hide();
        accepted();
        GameWindow gameWindow;
        gameWindow.exec();
    } else {
        Bookkeeping::B.clearAll();
        QMessageBox messageBox(this);
        messageBox.setText(QString("Too few players"));
        messageBox.exec();
    }
}

void StartWindow::setColor(int playerNumber)
{
    QColorDialog colorDialog(this);
    colorDialog.setModal(true);
    int accepted = colorDialog.exec();

    if(accepted == 1) {
        QColor selectedColor = colorDialog.selectedColor();
        QPalette pal = colorButtons_[playerNumber]->palette();
        pal.setColor(QPalette::Button, selectedColor);
        colorButtons_[playerNumber]->setPalette(pal);
    }
}
