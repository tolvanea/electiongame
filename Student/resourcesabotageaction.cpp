#include "resourcesabotageaction.h"

#include "actioninterface.h"
#include "agent.h"
#include "player.h"
#include "location.h"

#include "ownedinfluencecard.h"
#include "bookkeeping.h"
#include "statswidget.h"

#include <vector>
#include <QDebug>

using Interface::ActionInterface;
using Interface::Location;
using Interface::Player;
using Interface::Influence;
using Interface::CardInterface;
using std::shared_ptr;
using std::make_shared;

#include <memory>


ResourceSabotageAction::ResourceSabotageAction(shared_ptr<Agent> agent, shared_ptr<Agent> victim):
    agent_(agent),
    victim_(victim),
    location_(agent_->placement().lock()),
    owner_(agent_->owner().lock())
{

}

bool ResourceSabotageAction::canPerform() const
{
    // Checks the following:
    //      saboteur is on board
    //      victim is on same location
    //      victim is not own card

    bool agent_on_board = (location_ != nullptr);
    if (!agent_on_board) return false;

    bool correct_agent_type = (agent_->title() == "Saboteur");

    bool victim_on_same_location = (location_ == victim_->placement().lock());

    bool victim_not_own = (owner_ != victim_->owner().lock());

    return correct_agent_type && victim_on_same_location && victim_not_own;

}

void ResourceSabotageAction::perform()
{
    bool db =  true; //debug messages
    agent_->invariant2();
    victim_->invariant2();

    shared_ptr<Player> receiver = victim_->owner().lock();
    shared_ptr<OwnedInfluenceCard> negative_influence;
    if (location_->id() == 0)
        negative_influence = make_shared<OwnedInfluenceCard>(
                    "Nazi card", receiver, -3);
    else if (location_->id() == 1)
        negative_influence = make_shared<OwnedInfluenceCard>(
                    "TV bloober", receiver, -3);
    else if (location_->id() == 2)
        negative_influence = make_shared<OwnedInfluenceCard>(
                    "Bankruptcy", receiver, -3);
    else if (location_->id() == 3)
        negative_influence = make_shared<OwnedInfluenceCard>(
                    "Low on food chain", receiver, -3);


    Bookkeeping::B.giveInfluenceCard(receiver, negative_influence);
    if (db) qDebug()<<"        ResourceSabotageAction "<<negative_influence->name();

    Bookkeeping::B.getStatsWidget()->showText(0, "Gived " + negative_influence->name());
    Bookkeeping::B.getStatsWidget()->showText(1, "to " + negative_influence->owner().lock()->name());

    agent_->invariant2();
    victim_->invariant2();

}
