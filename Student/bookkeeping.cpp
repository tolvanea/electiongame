#include "bookkeeping.h"

#include "statswidget.h"

#include "player.h"
#include "location.h"
#include "gameexception.h"
#include "rangeexception.h"
#include "validateactions.h"

#include <memory>
#include <vector>
#include <map>
#include <exception>


using Interface::Location;
using Interface::Player;
using Interface::GameException;

using std::weak_ptr;
using std::vector;


void Bookkeeping::addPlayer(weak_ptr<const Player> player)
{
    players_.push_back(player);

    // Initialize players locational unlocks
    {
        if (locations_.size() != 4){
            throw Interface::GameException(
                    "Locations must be initialized before adding players!");
        }

        // At the beginning of game there's only social media unlocked
        std::map<weak_ptr<const Location>,
                 bool,
                 std::owner_less<weak_ptr<const Location>>> players_location_unlocks;
        for (auto location : locations_){
            if (location.lock()->name() == "Social media")
                players_location_unlocks.insert({location, true});
            else{
                players_location_unlocks.insert({location, false});
            }
        }
        avaible_locations_.insert({player, players_location_unlocks});
    }

    // Initialize players influence cards
    {
        vector<std::shared_ptr<OwnedInfluenceCard>> cards = {};
        influence_cards_.insert({player,cards});
    }
}

void Bookkeeping::addLocation(weak_ptr<const Interface::Location> location)
{
    locations_.push_back(location);
}

bool Bookkeeping::isUnlocked(weak_ptr<const Interface::Player> player, weak_ptr<const Interface::Location> location) const
{
    if (player.lock().get()==nullptr){
        qDebug()<<"Umm.. player is nullptr, crashing";
        throw GameException("Player is nullptr");
    }
    if (location.lock().get()==nullptr){
        qDebug()<<"Umm.. location is nullptr, crashing";
        throw GameException("Location is nullptr");
    }
    return avaible_locations_.at(player).at(location);
}

bool Bookkeeping::isNextLocked(weak_ptr<const Interface::Player> player, weak_ptr<const Interface::Location> location) const
{
    if (location.lock()->id() != 3){
        // Gets next location in 'locations_' vector
        // This is done by first searching current locations place in vector,
        // incrementing iterrator by one and then dereferensing to the value.
        // The std::find_if does the searching, and lambda function compares
        // weak_ptrs with '=='.
        weak_ptr<const Location> nextLocation =
                *(++std::find_if(
                      locations_.begin(),
                      locations_.end(),
                      [&](weak_ptr<const Location> l){
                            return l.lock()==location.lock();}
                 ));
        return !(avaible_locations_.at(player).at(nextLocation));
    }
    else{
        return false;
    }
}

void Bookkeeping::unlockNext(weak_ptr<const Interface::Player> player, weak_ptr<const Interface::Location> location)
{
    if (location.lock()->id() != 3){
        // Explanation of following 'nextLocation' is in 'Bookkeeping::isNextLocked()'
        weak_ptr<const Location> nextLocation =
                *(++std::find_if(
                      locations_.begin(),
                      locations_.end(),
                      [&](weak_ptr<const Location> l){
                            return l.lock()==location.lock();}
                 ));
        avaible_locations_.at(player).at(nextLocation) = true;
    }
    else{
        throw Interface::GameException("No next location to unlock");
    }

}

vector<std::shared_ptr<OwnedInfluenceCard> > Bookkeeping::influenceCards(weak_ptr<const Interface::Player> player) const
{
    try{
        return influence_cards_.at(player);
    }
    catch(std::out_of_range e){
        throw Interface::RangeException("Player not found");
    }
}


void Bookkeeping::giveInfluenceCard(weak_ptr<const Interface::Player> player, shared_ptr<OwnedInfluenceCard> card)
{
    try{
        influence_cards_.at(player).push_back(card);
    }
    catch(std::out_of_range e){
        throw Interface::RangeException("Player not found");
    }
}

int Bookkeeping::readSetting(QString key)
{
    try{
        return settings_.at(key);
    }
    catch(std::out_of_range){
        throw Interface::RangeException("No setting matces for given key");
    }
}

void Bookkeeping::storeSettings(std::map<QString, int> settings)
{
    settings_ = settings;
}

std::weak_ptr<Interface::Game> Bookkeeping::getGame()
{
    return game_;
}

void Bookkeeping::setGame(std::weak_ptr<Interface::Game> game)
{
    game_ = game;
}

int &Bookkeeping::actionsLeft()
{
    return actions_left_;
}

int Bookkeeping::turnsLeft() const
{
    return turns_left_;
}

void Bookkeeping::setTurns(int turns)
{
    turns_left_ = turns;
}

shared_ptr<ValidateActions> Bookkeeping::getValidateActions() const
{
    return validate_actions_;
}


void Bookkeeping::clearAll()
{
    players_.clear();
    locations_.clear();
    avaible_locations_.clear();
    influence_cards_.clear();
    statsWidget_ = nullptr;
    boardWidget_ = nullptr;
    board_ = nullptr;
    player_names_->clear();
    player_colors_->clear();
}

void Bookkeeping::updateStats() const
{
    if (statsWidget_ != nullptr) {
        statsWidget_->updateStats();
    }
    if (board_ != nullptr) {
        board_->update();
    }
    if (boardWidget_ != nullptr) {
        boardWidget_->update();
        boardWidget_->repaint();
    }
}

StatsWidget *Bookkeeping::getStatsWidget() const
{
    return statsWidget_;
}

void Bookkeeping::setStatsWidget(StatsWidget *statsWidget)
{
    statsWidget_ = statsWidget;
}

shared_ptr<vector<QString> > Bookkeeping::playerNames() const
{
    return player_names_;
}
shared_ptr<vector<QColor> > Bookkeeping::playerColors() const
{
    return player_colors_;
}

int Bookkeeping::totalInfluence(std::shared_ptr<Interface::Player> player) const
{
    try{
        int influence = 0;
        for (unsigned short i=0; i < 4; i++) {
            influence = influence + (i+1)*game_.lock()->locations()[i]->influence(player);
        }

        influence = influence * cardInfluence(player);
        return influence;

    } catch(GameException e) {
        qDebug() << "Error when fetching influence statistics:" << e.msg();
        // It's over nine thousand!
        // Actually, it's less, even though the absolute value is bigger
        return -9000;
    }
}

double Bookkeeping::cardInfluence(std::shared_ptr<Interface::Player> player) const
{
    try {
        double influence = 1.0;
        for (shared_ptr<OwnedInfluenceCard> influenceCard : Bookkeeping::B.influenceCards(player)) {
            influence = influence *(1 + 0.05*influenceCard->amount());
        }
        return influence;

    } catch(GameException e) {
    qDebug() << "Error when fetching influence statistics:" << e.msg();
    // It's over nine thousand!
    // Actually, it's less, even though the absolute value is bigger
    return -9000;
    }
}

void Bookkeeping::setBoard(Board *board)
{
    board_ = board;
}

void Bookkeeping::setBoardWidget(BoardWidget *boardWidget)
{
    boardWidget_ = boardWidget;
}

Bookkeeping::Bookkeeping():
    players_(),
    locations_(),
    avaible_locations_(),
    influence_cards_(),
    validate_actions_(std::make_shared<ValidateActions>()),
    player_names_(std::make_shared<vector<QString>>()),
    player_colors_(std::make_shared<vector<QColor>>())
{
}

Bookkeeping Bookkeeping::B;

