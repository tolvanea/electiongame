#ifndef DRAGMANAGER_H
#define DRAGMANAGER_H

#include "stateexception.h"

#include <QDebug>

#include <memory>

class Card;

/**
 * @brief The DragManager class manages the variables of a card dragging operation
 * The dragging system is heavily based on the Qt drag & drop framework implementations
 * found in the Card and CardSlot classes and their subclasses.
 */
class DragManager
{
public:
    DragManager();

    /**
     * @brief getCard returns the currently dragged card
     * @return pointer to the currently dragged card
     * @pre A drag is ongoing
     * @exception Interface::StateException if no drag is ongoing
     */
    Card *getCard() const;

    /**
     * @brief startDrag starts a new dragging event
     * @param card the card to be dragged
     * @param is_valid whether the move is valid within the game rules
     * @pre There is no drag ongoing
     * @post Exception guarantee: basic
     * @exception Interface::StateException if a drag is ongoing
     */
    void startDrag(Card *card, bool is_valid);

    /**
     * @brief endDrag ends the ongoing drag event
     * @post No drag event is occurring at the moment
     * @pre -
     */
    void endDrag();

    /**
     * @brief getDragging checks whether a drag event is ongoing
     * @return whether a drag is ongoing
     * @pre -
     * @post Exception guarantee: nothrow
     */
    bool getDragging() const;

    /**
     * @brief getValidGrab is for checking whether the current drag event is valid
     * within the game rules
     * @return whether the current drag event is valid within the game rules
     * @pre -
     * @post Exception guarantee: nothrow
     */
    bool getValidGrab() const;

private:
    Card *draggedCard_ = nullptr;
    bool dragging_ = false;
    bool valid_grab_ = false;

    void invariant() const;
};

#endif // DRAGMANAGER_H
