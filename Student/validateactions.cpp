#include "validateactions.h"

#include "cardinterface.h"
#include "location.h"
#include "gameexception.h"
#include "stateexception.h"
#include "bookkeeping.h"
#include "actioninterface.h"
#include "gamewindow.h"

#include "removeagentaction.h"
#include "resourcegatheringaction.h"
#include "resourcesabotageaction.h"
#include "influencegatheringaction.h"
#include "influencesabotageaction.h"
#include "connectiongatheringaction.h"
#include "connectionboostaction.h"
#include "sendaction.h"
#include "withdrawaction.h"

#include <iostream>
#include <QDebug>
#include <memory>

using Interface::CardInterface;
using Interface::ActionInterface;
using Interface::Location;
using std::shared_ptr;
using std::weak_ptr;
using std::make_shared;
using std::cout;
using std::endl;


bool db = false; //debug messages

ValidateActions::ValidateActions():
    current_action_(),
    no_turn_count_(false),
    card_state_change_(""),
    perform_no_action_(false)
{

}

bool ValidateActions::canGrab(QString slot_from, shared_ptr<Location> location_from, shared_ptr<CardInterface> card)
{
    bool has_actions_left = (Bookkeeping::B.actionsLeft() != 0);
    if (!has_actions_left) {
        if (db) qDebug()<<"    has_actions_left "<<has_actions_left;
        return false;
    }

    weak_ptr<Player> current_player = Bookkeeping::B.getGame().lock()->currentPlayer();

    if (slot_from == "hand"){
        if (db) qDebug()<<"    slot_from == hand";
        return true;
    }
    else if (slot_from == "location_slot"){
        bool is_own =
                (card->owner().lock() == current_player.lock());
        if (db) qDebug()<<"    is_own "<<is_own;
        return is_own;
    }
    else if (slot_from == "deck"){
        ResourceGatheringAction draw_card(current_player.lock(), location_from);
        if (db) qDebug()<<"    slot_from == deck"<<"draw_card.canPerform( )"
                       <<draw_card.canPerform();
        return draw_card.canPerform();
    }

    throw Interface::GameException("Unknown slot");
}







bool ValidateActions::canDrop(QString slot_from,
             QString slot_to,
             shared_ptr<Interface::Location> location_from,
             shared_ptr<Interface::Location> location_to,
             shared_ptr<Interface::CardInterface> card,
             shared_ptr<Interface::CardInterface> card_under)
{
    // This function test wether move is legal, and if it is, it saves the
    // action to private member, which it executes when 'drop()' is called.

    weak_ptr<Player> current_player =
            Bookkeeping::B.getGame().lock()->currentPlayer();

    bool card_underneath = (card_under != nullptr);
    bool is_agent = (card->typeName()=="Agent");
    shared_ptr<Agent> agent;
    shared_ptr<Agent> victim;
    if (card_underneath){
        victim = std::dynamic_pointer_cast<Agent>(card_under);
    }
    if (is_agent){
        agent = std::dynamic_pointer_cast<Agent>(card);
    }

    // firstly let's handle cases when card is not agent, so after that it can
    // be assumed that the card is agent
    {
        if (!is_agent){
            if (slot_from == "deck" && slot_to == "hand"){
                shared_ptr<ResourceGatheringAction> drawCard =
                        make_shared<ResourceGatheringAction>(current_player.lock(),
                                                             location_from);
                bool is_valid = !card_underneath && drawCard->canPerform();
                if (is_valid){
                    current_action_ = drawCard;
                    card_state_change_ = "move";
                }
                if (db) qDebug()<<"    deck-->hand"<<"is_valid: drawCard->canPerform()"<<is_valid;
                return is_valid;
            }
            else {
                if (db) qDebug()<<"    not_deck-->not_hand -> false";
                return false;
            }
        }
    }

    // it is always possible to draw cards to hand, if there's space
    if (slot_to == "hand"){

        if (slot_from == "hand"){
            bool is_valid =!card_underneath;
            if (is_valid){
                perform_no_action_ = true;
                no_turn_count_ = true;
                card_state_change_ = "move";
            }
            if (db) qDebug()<<"    hand-->hand"<<"!card_underneath"<<is_valid;
            return is_valid;
        }
        else if (slot_from == "location_slot"){
            shared_ptr<Interface::WithdrawAction> withdraw =
                    make_shared<Interface::WithdrawAction>(agent);
            bool is_valid = !card_underneath && withdraw->canPerform();
            if (is_valid){
                current_action_ = withdraw;
                card_state_change_ = "move";
            }
            if (db) qDebug()<<"    loc_slot-->hand"<<"is_valid: withdraw->canPerform()"
                           <<is_valid;

            return is_valid;
        }
        else if (slot_from == "deck"){
            shared_ptr<ResourceGatheringAction> drawCard =
                    make_shared<ResourceGatheringAction>(current_player.lock(),
                                                         location_from);
            bool is_valid = !card_underneath && drawCard->canPerform();
            if (is_valid){
                current_action_ = drawCard;
                card_state_change_ = "move";
            }
            if (db) qDebug()<<"    deck-->hand"<<"is_valid: drawCard->canPerform()"<<is_valid;
            return is_valid;
        }
    }


    // moving card to location slot
    else if (slot_to == "location_slot"){

        if (slot_from == "hand"){
            if (!card_underneath){
                shared_ptr<SendAction> send =
                        make_shared<SendAction>(agent, location_to);
                bool is_valid = send->canPerform();
                if (is_valid){
                    current_action_ = send;
                    card_state_change_ = "move";
                }
                if (db) qDebug()<<"    hand-->loc_slot"<<"send->canPerform()"<<send->canPerform();
                return is_valid;
            }
            else{
                if (agent->name() == "Hacker"){
                    shared_ptr<RemoveAgentAction> hack =
                            make_shared<RemoveAgentAction>(victim, agent);
                    bool is_valid = hack->canPerform();
                    if (is_valid){
                        current_action_ = hack;
                        card_state_change_ = "delete both cards";
                    }
                    if (db) qDebug()<<"    hand-->loc_slot"<<"hack->canPerform()"<<hack->canPerform();
                    return is_valid;
                }
                else if ((agent->name() == "Consult")){
                    shared_ptr<ConnectionBoostAction> give_connections =
                            make_shared<ConnectionBoostAction>(agent, victim);
                    bool is_valid = give_connections->canPerform();
                    if (is_valid){
                        current_action_ = give_connections;
                        card_state_change_ = "delete this card";
                    }
                    if (db) qDebug()<<"    hand-->loc_slot"<<"give_connections->canPerform()"
                                   <<give_connections->canPerform();
                    return is_valid;

                }
                else{
                    if (db) qDebug()<<"    hand-->loc_slot (over other card) false";
                    return false;
                }
            }
        }
        else if (slot_from == "location_slot"){
            if (!card_underneath){
                shared_ptr<SendAction> send =
                        make_shared<SendAction>(agent, location_to);
                bool is_valid = send->canPerform();
                if (is_valid){
                    current_action_ = send;
                    card_state_change_ = "move";
                    if (location_from == location_to)
                        no_turn_count_ = true;
                }
                if (db) qDebug()<<"    loc_slot-->loc_slot"<<"send->canPerform()"<<send->canPerform();
                return is_valid;
            }
            else{
                if (agent->title() == "Saboteur"){
                    shared_ptr<ResourceSabotageAction> give_bad_card =
                            make_shared<ResourceSabotageAction>(agent, victim);
                    bool is_valid = give_bad_card->canPerform();
                    if (is_valid){
                        current_action_ = give_bad_card;
                        card_state_change_ = "stay";
                    }
                    if (db) qDebug()<<"    loc_slot-->loc_slot"<<"give_bad_card->canPerform()"<<give_bad_card->canPerform();
                    return is_valid;
                }
                else{
                    if (db) qDebug()<<"    loc_slot-->loc_slot (over other card) false";
                    return false;
                }
            }
        }

        // it's never possible to draw directly from deck to board
        else if (slot_from == "deck"){
            if (db) qDebug()<<"    deck-->loc_slot -> false";
            return false;
        }

        else{
            if (db) qDebug()<<"    something else-->loc_slot -> false";
            return false;
        }
    }


    // player cant move card back to deck
    else if (slot_to == "deck"){
        if (db) qDebug()<<"    ?-->deck -> false";
        return false;
    }


    else if (slot_to == "influence"){
        if (slot_from == "location_slot"){
            if (agent->title() != "Saboteur"){
                shared_ptr<InfluenceGatheringAction> gather_influence =
                        make_shared<InfluenceGatheringAction>(agent);
                bool is_valid = gather_influence->canPerform();
                if (is_valid){
                    current_action_ = gather_influence;
                    card_state_change_ = "stay";
                }
                if (db) qDebug()<<"    loc_slot-->infl"<<"gather_influence->canPerform();"<<
                                  gather_influence->canPerform();
                return is_valid;
            }
            else { //saboteur
                shared_ptr<InfluenceSabotageAction> sabotage_influence =
                        make_shared<InfluenceSabotageAction>(agent);
                bool is_valid = sabotage_influence->canPerform();
                if (is_valid){
                    current_action_ = sabotage_influence;
                    card_state_change_ = "stay";
                }
                if (db) qDebug()<<"    loc_slot-->infl"<<"sabotage_influence->canPerform();"<<
                                  sabotage_influence->canPerform();
                return is_valid;
            }
        }
        else{
            if (db) qDebug()<<"   not_loc_slot-->influence -> false";
            return false;
        }
    }


    else if (slot_to == "connection"){
        if (slot_from == "location_slot"){
            shared_ptr<ConnectionGatheringAction> gather_connections =
                    make_shared<ConnectionGatheringAction>(agent);
            bool is_valid = !card_underneath && gather_connections->canPerform();
            if (is_valid){
                current_action_ = gather_connections;
                card_state_change_ = "stay";
            }
            if (db) qDebug()<<"    loc_slot-->connections"<<"is_valid: gather_connections->canPerform();"<<is_valid;
            return is_valid;
        }
        else{
            if (db) qDebug()<<"    (not loc_slot)-->connections -> false";
            return false;
        }
    }

    else if (slot_to == "trash"){
        if ((slot_from == "location_slot") || (slot_from == "hand")){
            shared_ptr<RemoveAgentAction> remove =
                    make_shared<RemoveAgentAction>(agent);
            bool is_valid = remove->canPerform();
            if (is_valid){
                current_action_ = remove;
                no_turn_count_ = true;
                card_state_change_ = "delete this card";
            }
            if (db) qDebug()<<"    (loc_slot||hand)-->trash"<<"remove->canPerform()"<<remove->canPerform();
            return is_valid;
        }
        else{
            if (db) qDebug()<<"    something else-->trash -> false";
            return false;
        }
    }
    else{
        if (db) qDebug()<<"Unkown state: "<<slot_from<<"-->"<<slot_to<<"\n";
        throw Interface::GameException("Move could not be checked");
        return false;

    }
    if (db) qDebug()<<"Unkown state: "<<slot_from<<"-->"<<slot_to<<"\n";
    throw Interface::GameException("Move could not be checked");
}









QString ValidateActions::drop()
{
    if (perform_no_action_){
        discardAction();
        return card_state_change_;
    }

    else if (current_action_ != nullptr){
        emit processAction(current_action_, no_turn_count_);
        // Currently no use of gamewindow pointer, if the same is signal-slotted
        // GameWindow* game_window_p = Bookkeeping::B.getGameWindowPointer();
        // game_window_p->actionPerformed(current_Action_, no_turn_count_);
        discardAction();
        return card_state_change_;
    }

    else{
        throw Interface::StateException("ValidateAction object has no action to perform");
    }
}

void ValidateActions::discardAction()
{
    current_action_ = nullptr;
    no_turn_count_ = false;
    perform_no_action_ = false;
}


