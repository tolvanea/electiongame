#include "gameinitialization.h"

#include "game.h"
#include "boardwidget.h"
#include "handwidget.h"
#include "influence.h"
#include "location.h"
#include "deckinterface.h"
#include "settingsreader.h"
#include "formatexception.h"
#include "ioexception.h"
#include "keyexception.h"
#include "gameexception.h"

#include "agent.h"
#include "bookkeeping.h"

#include <memory>
#include <vector>
#include <QDebug>


using Interface::Game;
using Interface::Influence;
using Interface::Location;
using Interface::Player;

using std::make_shared;
using std::shared_ptr;


namespace Initialization {

void createInfluenceCards(shared_ptr<Game> game){

    std::vector<std::shared_ptr<Location>> locations = game->locations();

    shared_ptr<Location> location_social_media = locations.at(0);
    shared_ptr<Location> location_media = locations.at(1);
    shared_ptr<Location> location_corporations = locations.at(2);
    shared_ptr<Location> location_party = locations.at(3);


    // Social media influence cards:
    shared_ptr<Influence> influence_twitter =
            make_shared<Influence>("Twitter user", location_social_media, 1);
    shared_ptr<Influence> influence_selfie =
            make_shared<Influence>("Selfie guy", location_social_media, 1);
    shared_ptr<Influence> influence_pop =
            make_shared<Influence>("9gag meme", location_social_media, 2);
    shared_ptr<Influence> influence_cambridge_analytica =
            make_shared<Influence>("Cambridge Analytica", location_social_media, 5);

    location_social_media->deck()->addCard(influence_twitter);
    location_social_media->deck()->addCard(influence_selfie);
    location_social_media->deck()->addCard(influence_pop);
    location_social_media->deck()->addCard(influence_cambridge_analytica);


    // Media influence cards:
    shared_ptr<Influence> influence_child =
            make_shared<Influence>("Friendly to children", location_media, 3);
    shared_ptr<Influence> influence_tolerant =
            make_shared<Influence>("Tolerant & Friendly", location_media, 2);
    shared_ptr<Influence> influence_poor =
            make_shared<Influence>("Honest intentions", location_media, 2);
    location_media->deck()->addCard(influence_child);
    location_media->deck()->addCard(influence_tolerant);
    location_media->deck()->addCard(influence_poor);


    // Corporatios influence cards:
    shared_ptr<Influence> influence_oil =
            make_shared<Influence>("Concession to oil", location_corporations, 3);
    shared_ptr<Influence> influence_tax =
            make_shared<Influence>("Tax deductions", location_corporations, 3);
    shared_ptr<Influence> influence_pollution =
            make_shared<Influence>("Emission permission", location_corporations, 4);

    location_corporations->deck()->addCard(influence_oil);
    location_corporations->deck()->addCard(influence_tax);
    location_corporations->deck()->addCard(influence_pollution);


    // Political party influence cards:
    shared_ptr<Influence> influence_chairman =
            make_shared<Influence>("Chairman's pal", location_party, 4);
    shared_ptr<Influence> influence_delegation =
            make_shared<Influence>("Delegate", location_party,4);
    shared_ptr<Influence> influence_promotion =
            make_shared<Influence>("Promotion", location_party, 5);

    location_party->deck()->addCard(influence_chairman);
    location_party->deck()->addCard(influence_delegation);
    location_party->deck()->addCard(influence_promotion);

    return;
}


void createSaboteursAgents(shared_ptr<Game> game){

    std::vector<std::shared_ptr<Location>> locations = game->locations();

    shared_ptr<Location> location_social_media = locations.at(0);
    shared_ptr<Location> location_media = locations.at(1);
    shared_ptr<Location> location_corporations = locations.at(2);
    shared_ptr<Location> location_party = locations.at(3);

    for (int i = 0; i<2;i++){
        shared_ptr<Agent> troll =
                make_shared<Agent>("Troll","Saboteur", false, 0);

        shared_ptr<Agent> journalist =
                make_shared<Agent>("Bribed journalist","Saboteur", false, 1);

        shared_ptr<Agent> business_shark =
                make_shared<Agent>("Business shark","Saboteur", false, 2);

        shared_ptr<Agent> manipulator =
                make_shared<Agent>("Manipulator","Saboteur", false, 3);

        location_social_media->deck()->addCard(troll);
        location_media->deck()->addCard(journalist);
        location_corporations->deck()->addCard(business_shark);
        location_party->deck()->addCard(manipulator);
            }
}


void createSupporters(shared_ptr<Game> game){
    std::vector<std::shared_ptr<Location>> locations = game->locations();

    shared_ptr<Location> location_social_media = locations.at(0);
    shared_ptr<Location> location_media = locations.at(1);
    shared_ptr<Location> location_corporations = locations.at(2);
    shared_ptr<Location> location_party = locations.at(3);

    // create and add 2 supporters to every local deck
    for (int i = 0; i<2;++i){
        for (int j = 0; j<4;++j){
            shared_ptr<Agent> supporter =
                    make_shared<Agent>("Supporter","Volunteer", false, j);
            locations.at(j)->deck()->addCard(supporter);
        }
    }
}



void createSpecialAgents(shared_ptr<Game> game){

    std::vector<std::shared_ptr<Location>> locations = game->locations();

    shared_ptr<Location> location_social_media = locations.at(0);
    shared_ptr<Location> location_media = locations.at(1);
    shared_ptr<Location> location_corporations = locations.at(2);
    shared_ptr<Location> location_party = locations.at(3);


    // create and add 2 hacker to every local deck
    for (int i = 0; i<2;++i){
        for (int j = 0; j<4;++j){
            shared_ptr<Agent> hacker =
                    make_shared<Agent>("Hacker","Services", true);

            locations.at(j)->deck()->addCard(hacker);
        }
    }

    // create 2 consultant cards to every local deck
    for (int i = 0; i<2;++i){
        for (int j = 0; j<4;++j){
            shared_ptr<Agent> consultant =
                    make_shared<Agent>("Consult","Services", true);

            locations.at(j)->deck()->addCard(consultant);
        }
    }
    // TODO make up more special cards, and add them to correct decks

}


void createGeneralAgents(shared_ptr<Player> player, QString player_name){
    shared_ptr<Agent> general_agent_1 =
            make_shared<Agent>(player_name,"Subordinate", true);
    shared_ptr<Agent> general_agent_2 =
            make_shared<Agent>(player_name,"Subordinate", true);

    general_agent_1->setOwner(player);
    general_agent_2->setOwner(player);

    player->addCard(general_agent_1);
    player->addCard(general_agent_2);

}

std::map<QString, int> readSettings()
{
    std::map<QString, int> default_settings =
        { {"MAX_PLAYERS", 4},
          {"TOTAL_ROUNDS",20},
          {"ACTIONS_FOR_TURN",3}
        };

    try{
        Interface::SettingsReader::READER.readSettings();
    }
    catch(Interface::GameException exp){
        qDebug() << "Settings error, using defaults, : " + exp.msg();
        return default_settings;
    }

    for (auto pair : default_settings){
        try{
            QString value = Interface::SettingsReader::READER.getValue(pair.first);
            bool conversion_succes;
            int value_num = value.toInt(&conversion_succes);
            if (conversion_succes)
                default_settings.at(pair.first) = value_num;

        }
        catch(Interface::KeyException exp){
            // if error occurs, use default settings
            qDebug() << "Settings error, using default value, : " + exp.msg();
        }
    }
    return default_settings;


}

} //Initialization
