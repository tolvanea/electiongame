#include "endwindow.h"
#include "ui_endwindow.h"

EndWindow::EndWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EndWindow)
{
    ui->setupUi(this);

    try {
        const unsigned short titlecount = 7;
        const QString titles[titlecount] = {
            QString("Player"),
            QString("Social media (") + QString(QChar(0x00D7)) + "1)",
            QString("Media (") + QString(QChar(0x00D7)) + "2)",
            QString("Companies (") + QString(QChar(0x00D7)) + "3)",
            QString("Parliament (") + QString(QChar(0x00D7)) + "4)",
            QString("Card multiplier"),
            QString("Total")
        };

        for(unsigned short i = 0; i < titlecount; i++) {
            QLabel *label = new QLabel(titles[i]);
            ui->gridLayout->addWidget(label, 0, i);
        }

        std::shared_ptr<Interface::Game> game = Bookkeeping::B.getGame().lock();

        int highestInfluence = std::numeric_limits<int>::min();
        QString winnerName;
        bool draw = false;

        for(unsigned short i = 0; i < Bookkeeping::B.playerNames()->size(); i++) {
            std::shared_ptr<Interface::Player> player = game->players().at(i);

            QLabel *nameLabel = new QLabel(player->name());
            ui->gridLayout->addWidget(nameLabel, i+1, 0);

            for(unsigned short location = 0; location < 4; location++) {
                QLabel *locationLabel = new QLabel(QString::number(game->locations().at(location)->influence(player)));
                ui->gridLayout->addWidget(locationLabel, i+1, location+1);
            }

            QLabel *cardLabel = new QLabel(QString::number(Bookkeeping::B.cardInfluence(player)));
            ui->gridLayout->addWidget(cardLabel, i+1, 5);

            int totalInfluence = Bookkeeping::B.totalInfluence(player);

            QLabel *totalLabel = new QLabel(QString::number(totalInfluence));
            ui->gridLayout->addWidget(totalLabel, i+1, 6);

            if(totalInfluence == highestInfluence) {
                draw = true;
            } else if(totalInfluence > highestInfluence) {
                draw = false;
                winnerName = player->name();
                highestInfluence = totalInfluence;
            }
        }
        if(draw)
            ui->winnerLabel->setText(QString("Draw"));
        else
            ui->winnerLabel->setText(QString("Winner: ") + winnerName);
    } catch(Interface::GameException e) {
        qDebug() << e.msg();
    }
}

EndWindow::~EndWindow()
{
    delete ui;
}
