#ifndef BOOKKEEPING_H
#define BOOKKEEPING_H

// #include "statswidget.h" //circular depedency
#include "board.h"
#include "boardwidget.h"
#include "location.h"
#include "player.h"
#include "ownedinfluencecard.h"

#include <memory>
#include <vector>
#include <map>


/**
 * @file
 * @brief This file contains general bookeeping singleton class.
 */


using Interface::Location;
using Interface::Player;

using std::weak_ptr;
using std::shared_ptr;
using std::vector;

class Board;
class BoardWidget;
// class GameWindow;
class StatsWidget;
class ValidateActions;

namespace Interface {
class Game;
}

/**
 * @brief The Bookkeeping class is a singleton class for keeping track on
 * properties that aren't implemented on cources side code. Also generally used
 * values are stored for global acces. This class includes extra info about
 * players and locations, and stores objects and pointers for easy access. As
 * static singletons stays in memory the the whole time, one must be careful, for
 * example, when using shared pointers.
 */
class Bookkeeping
{
public:
    /**
     * @brief B once defined singleton object
     */
    static Bookkeeping B;

    /**
      * @brief default destructor, remeber singleton is preserved the whole time
      * program is running
      */
    ~Bookkeeping() = default;

    /**
     * @brief add_location adds new location to bookkeeping
     * @param location weak pointer to location
     * @pre location is the same as on board, and is not already added
     * @post location is added in inner bookkeeping
     */
    void addLocation(weak_ptr<const Location> location);

    /**
     * @brief add_player adds new player to bookkeeping
     * @param player weak pointer to player
     * @pre all locations are initialized, and that player is not already added
     * @post player is added bookkeeping
     */
    void addPlayer(weak_ptr<const Player> player);

    /**
     * @brief is_unlocked checks if location is unlocked for that player
     * @param player
     * @param location
     * @pre player and location pointers are valid and they exist in bookkeping
     * @return
     */
    bool isUnlocked(weak_ptr<const Player> player, weak_ptr<const Location> location) const;

    /**
     * @brief nextIsLocked check whether next location to current is locked
     * @param player
     * @param location previous location
     * @return true/false, if location last one (political party), then return false
     */
    bool isNextLocked(weak_ptr<const Player> player, weak_ptr<const Location> location) const;

    /**
     * @brief unlockNext unlocks next location for player in previous place
     * @param player
     * @param location previous location to which next one is
     * @pre player and location pointers are valid and they exist in bookkeping
     * @pre game is initialized
     * @post location is unlocked for that player
     * @exception GameException if last location and no next to unlock
     */
    void unlockNext(weak_ptr<const Player> player, weak_ptr<const Location> location);

    /**
     * @brief influenceCards returns players gathered influece cards. Notice
     * Bookkeping is owner of OwnedInfluenceCard shared pointer
     * @param player shared_ptr to player
     * @return vector of OwnedInfluenceCard:s
     * @pre player pointer is valid and it exist in bookkeping
     */

    vector<shared_ptr<OwnedInfluenceCard>> influenceCards(weak_ptr<const Player> player)const;

    /**
     * @brief giveInfluenceCard gives and stores a new influece card for player
     * @param player
     * @param card
     * @pre player pointer is valid and it exist in bookkeping
     * @post influence card added for player in bookkeeping
     */
    void giveInfluenceCard(weak_ptr<const Player> player, shared_ptr<OwnedInfluenceCard> card);

    /**
     * @brief readSetting reads settings' stored values
     * @param key QString name to corresponding value
     * @return int value
     */
    int readSetting(QString key);

    /**
     * @brief storeSettings Stores settings
     * @param settings map of key-setting-pairs
     * @post settings stored
     */
    void storeSettings(std::map<QString, int> settings);

    /**
     * @brief getGame returns weak pointer to game
     * @pre game has been set
     * @return
     */
    std::weak_ptr<Interface::Game> getGame();

    /**
     * @brief setGame sets the game weak pointer
     * @param game
     * @post game pointer is set for 'getGame()'
     */
    void setGame(std::weak_ptr<Interface::Game> game);

    /**
     * @brief actionsLeft returns a referece to number of actions current player
     * has on his turn left.
     * @return
     */
    int& actionsLeft();

    /**
     * @brief turnsLeft returns a referece to number of turns game has left.
     * This is number of weeks to election
     * @return
     */
    int turnsLeft() const;

    /**
     * @brief setTurns sets turns for turnsLeft.
     * @param turns
     * @post turns are set
     */
    void setTurns(int turns);

    /**
     * @brief getValidateActions gets the pointer to validate actions object
     * @return
     */
    shared_ptr<ValidateActions> getValidateActions() const;


    /**
     * @brief clear_all clears all bookkeeping containers. As singletons can't be
     * destructed, this function must be called before starting a new game.
     * @post all containers are empty
     */
    void clearAll();

    /**
     * @brief updateStats tells the StatsWidget to redraw its contents
     */
    void updateStats() const;

    /**
     * @brief getStatsWidget returns pointer to gamewindows statswidget.
     * @return
     */
    StatsWidget *getStatsWidget() const;

    /**
     * @brief setStatsWidget sets the stats widget that will be updated using the
     * updateStats function
     * @param statsWidget pointer to the StatsWidget
     */
    void setStatsWidget(StatsWidget *statsWidget);

    /**
     * @brief playerNames returns shared pointer to vector which is supposed to
     * be filled with player names
     * @return
     */
    shared_ptr<vector<QString>>  playerNames() const;

    /**
     * @brief playerColors returns shared pointer to vector which is supposed to
     * contain personal colors for players.
     * @return
     */
    shared_ptr<vector<QColor> > playerColors() const;

    /**
     * @brief totalInfluence returns the total influence of a player
     * @param player the player for which the influence is calculated
     * @pre the game object exists
     * @return total influence for the player
     */
    int totalInfluence(std::shared_ptr<Interface::Player> player) const;

    /**
     * @brief cardInfluence returns the influence factor a player has collected
     * from cards
     * @param player the player for which the influence is calculated
     * @pre the game object exists
     * @return card-based influence for the player
     */
    double cardInfluence(std::shared_ptr<Interface::Player> player) const;

    /**
     * @brief setBoard sets board for updating board's stats (influence)
     * @param board_
     * @pre -
     * @post Exception quarantee: nothrow
     */
    void setBoard(Board *board_);

    /**
     * @brief setBoardWidget sets the BoardWidget associated with the updateStats() method
     * @param boardWidget the boardWidget which should be kept updated
     * @pre -
     * @post Exception quarantee: nothrow
     */
    void setBoardWidget(BoardWidget *boardWidget);


private:
    Bookkeeping();
    Bookkeeping(const Bookkeeping&) = delete;
    Bookkeeping& operator =(const Bookkeeping&) = delete;

    vector<weak_ptr<const Player>> players_;
    vector<weak_ptr<const Location>> locations_;

    // notice the third term in map is a sort algorithm for weak_ptrs
    std::map<
        weak_ptr<const Player>,
        std::map<
            weak_ptr<const Location>,
            bool,
            std::owner_less<weak_ptr<const Location>>
        >,
        std::owner_less<weak_ptr<const Player>>
    > avaible_locations_;

    std::map<
        weak_ptr<const Player>,
        vector<shared_ptr<OwnedInfluenceCard>>,
        std::owner_less<weak_ptr<const Player>>
    > influence_cards_;

    std::map<QString,int> settings_;
    std::weak_ptr<Interface::Game> game_;

    int actions_left_;
    int turns_left_;

    //GameWindow* GameWindow_pointer_;
    shared_ptr<ValidateActions> validate_actions_;

    StatsWidget *statsWidget_;
    BoardWidget *boardWidget_;
    Board *board_;

    shared_ptr<vector<QString>> player_names_;
    shared_ptr<vector<QColor>> player_colors_;

};

#endif // BOOKKEEPING_H
