#include "cardslot.h"

// Color origins: Twitter logo, Aamulehti.fi, Verkkokauppa.com, Pirate party website
const QColor CardSlot::colors_[4] = { QColor(29, 161, 242), QColor(0, 79, 157), QColor(227, 14, 27), QColor(81, 36, 131) };

const QString DiscardSlot::labels_[4] = { QString("4Chan"), QString("MV-lehti"), QString("OneCoin"), QString("NSDAP") };


CardSlot::CardSlot(std::shared_ptr<DragManager> dragManager, std::shared_ptr<Interface::Location> location)
{
    dragManager_ = dragManager;
    location_ = location;
    setAcceptDrops(true);

    if (location == nullptr) {
        color_ = Qt::lightGray;
    } else {
        color_ = colors_[location->id()];
    }
}

QRectF CardSlot::boundingRect() const
{
    return QRectF(0, 0, width_, height_);
}

void CardSlot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::black);
    painter->drawRect(boundingRect());

    if (dragOver_)
        painter->setBrush(color_.lighter());
    else
        painter->setBrush(color_);
    painter->drawRect(0.1*width_, 0.1*height_, 0.8*width_, 0.8*height_);
}

void CardSlot::setCard(Card *card)
{
    card_ = card;
}

Card *CardSlot::getCard() const
{
    return card_;
}

std::shared_ptr<Interface::Location> CardSlot::getLocation() const
{
    return location_;
}

QString CardSlot::getType() const
{
    return typeName_;
}

void CardSlot::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    event->setAccepted(true);
    if (event->mimeData()->text() == "card" && card_ == nullptr) {

        Card *card = dragManager_->getCard();
        if ( canDrop(card) ) {
            dragOver_ = true;
            update();
        }
    }
    invariant();
}

void CardSlot::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
    event->setAccepted(true);
    dragOver_ = false;
    update();
    invariant();
}

void CardSlot::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    invariant();

    dragOver_ = false;
    if (event->mimeData()->text() == "card") {
        Card *card = dragManager_->getCard();

        if( card->getSlot() == this )
            return;

        if( canDrop(card) ) {
            Bookkeeping::B.getValidateActions()->drop();
            performMove(card);
        }
    }
    update();
    invariant();
}

bool CardSlot::canDrop(Card *card) const
{
    try {
        // qDebug() << typeName_;
        shared_ptr<ValidateActions> va = Bookkeeping::B.getValidateActions();
        va->discardAction();
        bool validDrop = va->canDrop(card->getSlot()->getType(), getType(), card->getSlot()->getLocation(), location_, card->getGameCard(), nullptr);
        // qDebug() << "Can drop:" << validDrop;
        return validDrop;
    } catch(Interface::GameException e) {
        qDebug() << "Exception occurred when testing card dropping on slot:" << e.msg();
        return false;
    }
}

void CardSlot::performMove(Card *card)
{
    // Set card to correct scene
    QGraphicsScene *oldScene = card->scene();

    if (oldScene != scene()) {
        oldScene->removeItem(card);
        oldScene->update();
        scene()->addItem(card);
    }

    CardSlot *oldSlot = card->getSlot();
    qDebug() << "Moving card" << card << "from" << oldSlot << "to" << this;

    card->setPos(pos());
    card->setHidden(false);
    card_ = card;

    setCard(card);
    if (oldSlot != nullptr)
        oldSlot->setCard(nullptr);
    card->setSlot(this);

    invariant();
}

void CardSlot::invariant() const
{
    /*
    if(card_ != nullptr) {
        Q_ASSERT(card_->getSlot() == this);
    }
    */
    if (card_ != nullptr) {
        if(card_->getSlot() != this) {
            throw Interface::StateException(QString("CardSlot invariant failed"));
        }
    }
}

DiscardSlot::DiscardSlot(std::shared_ptr<DragManager> dragManager, std::shared_ptr<Interface::Location> location) : CardSlot::CardSlot(dragManager, location)
{
    typeName_ = QString("trash");

    if (location == nullptr) {
        color_ = Qt::red;
    } else {
        color_ = colors_[location->id()].darker();
    }
}

void DiscardSlot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::black);
    painter->drawRect(0, 0, width_, height_);

    if (dragOver_)
        painter->setBrush(color_);
    else
        painter->setBrush(color_.darker());
    painter->drawRect(0.1*width_, 0.1*height_, 0.8*width_, 0.8*height_);

    painter->setPen(Qt::black);
    painter->setFont(labelFont_);
    painter->drawText(20, 45, labels_[location_->id()]);
}

void DiscardSlot::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    dragOver_ = false;

    if (event->mimeData()->text() == "card") {
        Card *card = dragManager_->getCard();

        if(canDrop(card)) {
            Bookkeeping::B.getValidateActions()->drop();

            CardSlot *oldSlot = card->getSlot();

            qDebug() << "Destroying card" << card << "from" << oldSlot << "at" << this;
            card->scene()->removeItem(card);
            if (oldSlot != nullptr) {
                oldSlot->setCard(nullptr);
            }

            // Card deleting is implemented using the card itself
            // delete card;
            card->destroy();
        } else {
            // qDebug() << "Card cannot be destroyed";
        }
    }
    update();
}


DeckSlot::DeckSlot(std::shared_ptr<DragManager> dragManager, std::shared_ptr<Interface::Location> location) : CardSlot::CardSlot(dragManager, location)
{
    typeName_ = QString("deck");

    if (location == nullptr) {
        color_ = Qt::red;
    } else {
        color_ = colors_[location->id()].lighter();
    }
}

void DeckSlot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    CardSlot::paint(painter, option, widget);

    if (card_ == nullptr && location_->deck()->size() > 0) {
        std::shared_ptr<Interface::CardInterface> card = location_->deck()->peek();
        // qDebug() << card->typeName();

        if (card->typeName() == "Agent") {
            AgentCard *drawableCard = new AgentCard(dragManager_, std::static_pointer_cast<Agent>(card));
            // drawableCard->setOriginalLocation(location_->id());
            drawableCard->setPos(pos());
            drawableCard->setSlot(this);
            setCard(drawableCard);
            drawableCard->setHidden(true);
            scene()->addItem(drawableCard);

        } else if (card->typeName() == "Influence") {
            InfluenceCard *drawableCard = new InfluenceCard(dragManager_, std::static_pointer_cast<Interface::Influence>(card));
            // drawableCard->setOriginalLocation(location_->id());
            drawableCard->setPos(pos());
            drawableCard->setSlot(this);
            setCard(drawableCard);
            drawableCard->setHidden(true);
            scene()->addItem(drawableCard);

        } else {
            qDebug() << "ERROR: unknown card" << card.get();
        }
    }
}

void DeckSlot::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    event->ignore();
}

void DeckSlot::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
    event->ignore();
}

void DeckSlot::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    event->ignore();
}

HandSlot::HandSlot(std::shared_ptr<DragManager> dragManager) : CardSlot::CardSlot(dragManager, nullptr)
{
    typeName_ = QString("hand");
}

void HandSlot::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    event->setAccepted(true);
    Card *card = dragManager_->getCard();

    // Cards can be moved freely in hand
    if ( card->getSlot()->getType() == getType() ) {
        dragOver_ = true;
    } else {
        CardSlot::dragEnterEvent(event);
    }
    update();
    invariant();
}

void HandSlot::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    invariant();

    if (event->mimeData()->text() == "card") {
        event->setAccepted(true);

        Card *card = dragManager_->getCard();

        if ( card->getSlot() == this )
            return;

        // Cards can be moved freely in hand
        if ( card->getSlot()->getType() == getType() ) {
            performMove(card);
        } else {
            CardSlot::dropEvent(event);
        }
    }
    update();
    invariant();
}


ActionSlot::ActionSlot(std::shared_ptr<DragManager> dragManager) : CardSlot::CardSlot(dragManager, nullptr)
{
    width_ = 400;
    height_ = 360;
}

QRectF ActionSlot::boundingRect() const
{
    return QRect(-0.5*width_, -0.5*height_, width_, height_);
}

void ActionSlot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(Qt::red);
    painter->setBrush(color_);
    painter->drawRect(boundingRect());

}

void ActionSlot::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    dragOver_ = false;

    if (event->mimeData()->text() == "card") {
        Card *card = dragManager_->getCard();

        if ( canDrop(card) ) {
            Bookkeeping::B.getValidateActions()->drop();
        }

        CardSlot *oldSlot = card->getSlot();
        qDebug() << "Upgrading card" << card << "from" << oldSlot << "at" << this << "for" << typeName_;
    }
    update();
}

ConnectionSlot::ConnectionSlot(std::shared_ptr<DragManager> dragManager) : ActionSlot(dragManager)
{
    color_ = Qt::green;
    typeName_ = QString("connection");
}

void ConnectionSlot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    /*
    painter->setPen(Qt::green);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(boundingRect());
    */

    painter->setPen(QPen(Qt::black, 15));
    if (dragOver_)
        painter->setBrush(color_.lighter());
    else
        painter->setBrush(color_);
    painter->drawPie(QRect(-0.5*width_, -0.5*height_, 2*width_, height_), 100*16, 160*16);

    painter->drawPixmap(-130, -130, 260, 260, QPixmap(":/chairmans_pal.png"));
}

InfluenceSlot::InfluenceSlot(std::shared_ptr<DragManager> dragManager) : ActionSlot(dragManager)
{
    color_ = Qt::blue;
    typeName_ = QString("influence");
}

void InfluenceSlot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    /*
    painter->setPen(Qt::blue);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(boundingRect());
    */

    painter->setPen(QPen(Qt::black, 15));
    if (dragOver_)
        painter->setBrush(color_.lighter());
    else
        painter->setBrush(color_);
    painter->drawPie(QRect(-1.5*width_, -0.5*height_, 2*width_, height_), 80*16, -160*16);

    painter->drawPixmap(-130, -160, 260, 260, QPixmap(":/influence.png"));
}
