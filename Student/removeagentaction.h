#ifndef REMOVEAGENTACTION_H
#define REMOVEAGENTACTION_H


#include "actioninterface.h"
#include "agent.h"
#include "player.h"
#include "location.h"

#include <memory>

/**
 * @file
 * @brief Action class for removing agent from board
 */

/**
 * @brief The RemoveAgentAction class implements agent removal action. This can
 * be performed both by player to his own cards, and by hacker to someone else's
 * cards.
 */
class RemoveAgentAction: public Interface::ActionInterface
{
public:
    /**
     * @brief RemoveAgentAction constructor
     * @param victim shared pointer to the agent being removed
     * @param hacker shared pointer to hacker-card, if player removes his own
     * card, then this is nullptr
     * @pre -
     * @post remove action created
     */
    RemoveAgentAction(std::shared_ptr<Agent> victim,
                      std::shared_ptr<Agent> hacker = nullptr);

    /**
     * @brief ~RemoveAgentAction destructor
     */
    virtual ~RemoveAgentAction() = default;

    /**
     * @brief canPerform
     * @pre -
     * @return true, iff first agent is hacker and second one is not subordinate
     * @post exception gurantee: nothrow
     */
    virtual bool canPerform() const;

    /**
     * @brief perform removes victim and hacker from the game
     * @pre canPerform() == true
     * @post exception gurantee: basic
     */
    virtual void perform();

private:
    std::shared_ptr<Agent> hacker_;
    std::shared_ptr<Agent> victim_;
    std::shared_ptr<Interface::Location> location_;
};

#endif // REMOVEAGENTACTION_H
