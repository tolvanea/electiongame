#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "aboutwindow.h"
// #include "gamewindow.h"
#include "startwindow.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class is the first window to be loaded when the program starts
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    /**
     * @brief startGame creates the game window and hides the main window until the game window is closed
     */
    void startGame();

    /**
     * @brief showAbout shows the about window
     */
    void showAbout();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
