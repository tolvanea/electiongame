#include "gamewindow.h"

#include "deckinterface.h"
#include "game.h"
#include "influence.h"
#include "location.h"
#include "agent.h"
#include "player.h"
#include "runner.h"
#include "manualcontrol.h"
#include "controlinterface.h"
#include "actioninterface.h"

#include "boardwidget.h"
#include "dragmanager.h"
#include "handwidget.h"
#include "bookkeeping.h"
#include "gameinitialization.h"
#include "validateactions.h"

#include <QDebug>
#include <QListView>
#include <QtWidgets>
#include <memory>
#include <vector>
#include <iostream>

using Interface::Game;
using Interface::Influence;
using Interface::Location;
using Interface::Player;
using Interface::ManualControl;
using Interface::ControlInterface;
using Interface::ActionInterface;

using std::make_shared;
using std::shared_ptr;




GameWindow::GameWindow(QWidget *parent) :
    QDialog(parent)
{
    qDebug() << "Creating GameWindow";
    setWindowFlags(Qt::Window);

    createEngine();
    startGame();
    createWidgets();
}

GameWindow::~GameWindow()
{
    qDebug() << "Destroying GameWindow";
}


void GameWindow::actionPerformed(std::shared_ptr<Interface::ActionInterface> action, bool no_turn_count)
{
    bool db =  true; //debug messages
    if (db) qDebug()<<"Performing action";

    if (Bookkeeping::B.actionsLeft() <= 0){
        Q_ASSERT(false && "Make sure when no turns left, no actions to perform!");
    }

    shared_ptr<ManualControl> manual_control =
            std::dynamic_pointer_cast<ManualControl>(current_players_control_);

    manual_control->setNextAction(action);

    current_players_control_ = runner_->run();

    if (! no_turn_count)
        Bookkeeping::B.actionsLeft() -= 1;
    qDebug()<<game_->currentPlayer()->name()<<"'s action performed, "<<" turns left:"<<Bookkeeping::B.actionsLeft();

    handWidget_->update();
    boardWidget_->update();
}

void GameWindow::changeTurn()
{
    // change player every third action, notice: player has to click button for that
    Bookkeeping::B.setTurns(Bookkeeping::B.turnsLeft() - 1);
    if (Bookkeeping::B.turnsLeft() == 0){
        qDebug()<<" ";
        qDebug()<<"################ Game has come to its end #######################";
        qDebug()<<" ";
        endGame();
        return; // this 'return' should never be executed
    }
    game_->nextPlayer();
    current_players_control_ = runner_->run();
    Bookkeeping::B.actionsLeft() = Bookkeeping::B.readSetting("ACTIONS_FOR_TURN");
    qDebug()<<"Turn changed to "<<game_->currentPlayer()->name()<<", rounds left:"<<Bookkeeping::B.turnsLeft();

    handWidget_->update();
    boardWidget_->update();
    boardWidget_->repaint();
    handWidget_->repaint();
}

void GameWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Escape)
        return;
    else
        QDialog::keyPressEvent(event);
}

void GameWindow::createEngine()
{

    std::map<QString, int> settings = Initialization::readSettings();
    Bookkeeping::B.storeSettings(settings);


    qDebug() << "Creating the Game object";

    // create a game object
    game_ = make_shared<Game>();
    // qDebug() << game_.get();

    // create runner object
    runner_ = make_shared<Interface::Runner>(game_);

    // set up locations of the board
    {
        // create and initialize a location, and add it to the game
        shared_ptr<Location> location_social_media = make_shared<Location>(0, "Social media");
        shared_ptr<Location> location_media = make_shared<Location>(1, "Media");
        shared_ptr<Location> location_corporations = make_shared<Location>(2, "Corporation");
        shared_ptr<Location> location_party = make_shared<Location>(3, "Political party");

        location_social_media->initialize();
        location_media->initialize();
        location_corporations->initialize();
        location_party->initialize();

        game_->addLocation(location_social_media);
        game_->addLocation(location_media);
        game_->addLocation(location_corporations);
        game_->addLocation(location_party);


        // set up cards for the location deck
        {
            // create influence cards and add them to local decks
            Initialization::createInfluenceCards(game_);

            // create negative action agents and add them in local decks
            Initialization::createSaboteursAgents(game_);

            // create supporters
            Initialization::createSupporters(game_);

            // create general special agents and add them in local decks
            Initialization::createSpecialAgents(game_);

            // shuffle local decks
            location_social_media->deck()->shuffle();
            location_media->deck()->shuffle();
            location_corporations->deck()->shuffle();
            location_party->deck()->shuffle();
        }

        // set up external bookkeeping on game locations
        {
            Bookkeeping::B.addLocation(location_social_media);
            Bookkeeping::B.addLocation(location_media);
            Bookkeeping::B.addLocation(location_corporations);
            Bookkeeping::B.addLocation(location_party);
        }
    }



    // set up players
    {
        // add players to the game
        for (QString name : *Bookkeeping::B.playerNames()){
            // qDebug() << game_->players().size();
            shared_ptr<Player> player = game_->addPlayer(name);

            Initialization::createGeneralAgents(player, name);

            // add control to players
            shared_ptr<ManualControl> control = make_shared<ManualControl>();
            runner_->setPlayerControl(player, control);

            // add players to bookkeeping
            Bookkeeping::B.addPlayer(player);

        }   // extra info: players-vector can be accessed via 'game->players()'
    }

    Bookkeeping::B.setTurns((*Bookkeeping::B.playerNames()).size() *
                            Bookkeeping::B.readSetting("TOTAL_ROUNDS"));
    qDebug()<< (*Bookkeeping::B.playerNames()).size()<<"  "<<Bookkeeping::B.readSetting("TOTAL_ROUNDS")<<Bookkeeping::B.turnsLeft();

    Bookkeeping::B.setGame(game_);

    connectSingnals();

    qDebug() << "Game created";
}

void GameWindow::createWidgets()
{
    QGridLayout *layout = new QGridLayout;

    dragManager_ = std::make_shared<DragManager>();

    // Primary widgets
    boardScene_ = new QGraphicsScene(0, 0, 3000, 2000);
    handScene_ = new QGraphicsScene(0, 0, 1000, 290);

    boardWidget_ = new BoardWidget(boardScene_, dragManager_, game_);
    handWidget_ = new HandWidget(handScene_, dragManager_, game_);

    statsWidget_ = new StatsWidget(this, game_);

    // Buttons
    QVBoxLayout *buttonLayout = new QVBoxLayout();

    /*
    QPushButton *testButton = new QPushButton();
    testButton->setText("Test");
    testButton->show();
    buttonLayout->addWidget(testButton);
    connect(testButton, &QPushButton::clicked, this, &GameWindow::endGame);
    */

    changeTurnButton_ = new QPushButton();
    changeTurnButton_->setMinimumHeight(70);
    // changeTurnButton_->setMaximumWidth(200);
    changeTurnButton_->setText("End turn");
    changeTurnButton_->show();
    buttonLayout->addWidget(changeTurnButton_);

    /*
    QPushButton *clearHandButton = new QPushButton();
    clearHandButton->setText("Clear hand");
    clearHandButton->show();
    buttonLayout->addWidget(clearHandButton);

    QPushButton *loadHandButton = new QPushButton();
    loadHandButton->setText("Load hand");
    loadHandButton->show();
    buttonLayout->addWidget(loadHandButton);
    */

    layout->addLayout(buttonLayout, 1, 1);

    connect(changeTurnButton_, &QPushButton::clicked, this, &GameWindow::changeTurn);
    connect(changeTurnButton_, &QPushButton::clicked, []{Bookkeeping::B.updateStats();});
    connect(changeTurnButton_, &QPushButton::clicked, handWidget_, &HandWidget::loadHand);

    /*
    connect(clearHandButton, &QPushButton::clicked, handWidget_, &HandWidget::clearHand);
    connect(loadHandButton, &QPushButton::clicked, handWidget_, &HandWidget::loadHand);
    */

    layout->addWidget(boardWidget_, 0, 0);
    layout->addWidget(handWidget_, 1, 0);
    layout->addWidget(statsWidget_, 0, 1);
    setLayout(layout);

    statsWidget_->updateStats();
    handWidget_->loadHand();
}

void GameWindow::connectSingnals()
{
    shared_ptr<ValidateActions> ValidateActions_ptr = Bookkeeping::B.getValidateActions();
    connect(ValidateActions_ptr.get(), &ValidateActions::processAction, this, &GameWindow::actionPerformed);
}

void GameWindow::startGame()
{
    Bookkeeping::B.actionsLeft() = Bookkeeping::B.readSetting("ACTIONS_FOR_TURN");
    game_->setActive(true);
    current_players_control_ = runner_->run();

}

void GameWindow::endGame()
{
    game_->setActive(false);

    changeTurnButton_->setEnabled(false);
    EndWindow endWindow;
    endWindow.exec();
}
