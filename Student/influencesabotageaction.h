#ifndef INFLUENCESABOTAGEACTION_H
#define INFLUENCESABOTAGEACTION_H

#include "actioninterface.h"
#include "agent.h"
#include "location.h"
#include "player.h"


#include <memory>

/**
 * @file
 * @brief Action class for saboteurs to decrease other players' influence.
 */

using Interface::ActionInterface;
using Interface::Location;
using Interface::Player;
using std::shared_ptr;


/**
 * @brief The InfluenceSabotageAction class implements sabouteurs' action to lower
 * other players' influence on that field. Players' influence is decreaced by some
 * factor which is determinen by saboteurs connections.
 */
class InfluenceSabotageAction: public ActionInterface
{
public:
    /**
     * @brief InfluenceSabotageAction constructor
     * @param agent shared pointer to agent
     * @param victim the agent on whom the sabouteur acts
     * @pre -
     * @post action created
     */
    InfluenceSabotageAction(std::shared_ptr<Agent> agent);

    /**
     * @brief ~InfluenceSabotageAction destructor
     */
    virtual ~InfluenceSabotageAction() = default;

    /**
     * @brief canPerform checks whether it is possible agent to decrease influence
     * @pre -
     * @return true, iff agent is saboteur and there are other players' agents
     * on that location.
     * @post exception gurantee: nothrow
     */
    virtual bool canPerform() const;

    /**
     * @brief perform decreases other players' influence by some factor. Takes
     * in account saboteurs connections. The decrease is performed per every agent
     * on that field.
     * @pre canPerform() == true
     * @post exception gurantee: basic
     */
    virtual void perform();

private:
    shared_ptr<Agent> agent_;
    shared_ptr<Agent>  victim_;
    shared_ptr<Location> location_;
    shared_ptr<Player> owner_;
};

#endif // INFLUENCESABOTAGEACTION_H
