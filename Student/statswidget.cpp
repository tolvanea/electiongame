#include "statswidget.h"

StatsWidget::StatsWidget(QWidget *parent, std::shared_ptr<Interface::Game> game) : QWidget(parent)
{
    game_ = game;

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    layout_ = new QVBoxLayout;
    setLayout(layout_);

    setupPrimary();
    setupInfluence();
    setupText();

    Bookkeeping::B.setStatsWidget(this);

    updateStats();
    show();

    // showText(0, QString("foo"));
    // showText(1, QString("bar"));
}

StatsWidget::~StatsWidget()
{
    Bookkeeping::B.setStatsWidget(nullptr);
}

void StatsWidget::setupPrimary()
{
    QGroupBox *primaryBox = new QGroupBox(this);
    primaryBox->setAlignment(Qt::AlignHCenter);
    primaryBox->setTitle("Statistics");

    QGridLayout *layout = new QGridLayout;

    QLabel *influenceLabel = new QLabel(QString("Influence"));
    layout->addWidget(influenceLabel, 0, 1);

    QLabel *cardCountLabel = new QLabel(QString("Cards"));
    layout->addWidget(cardCountLabel, 0, 2);

    playerCount_ = game_->players().size();

    for (unsigned short i = 0; i < playerCount_; i++) {
        QLabel *nameLabel = new QLabel(game_->players()[i]->name());
        layout->addWidget(nameLabel, i+1, 0);

        QLabel *influenceLabel = new QLabel(QString("-"));
        influenceLabels_.push_back(influenceLabel);
        layout->addWidget(influenceLabel, i+1, 1);

        QLabel *cardLabel = new QLabel(QString("-"));
        cardCountLabels_.push_back(cardLabel);
        layout->addWidget(cardLabel, i+1, 2);
    }

    unsigned short labelcount = 3;
    const QString names[labelcount] = { QString("Current player"), QString("Actions left"), QString("Weeks left") };

    QLabel *lineLabel = new QLabel(QString(" "));
    layout->addWidget(lineLabel, playerCount_+1, 0);

    for (unsigned short i = 0; i < labelcount; i++) {
        QLabel *label = new QLabel(names[i]);
        layout->addWidget(label, playerCount_ + 2 + i, 0);
    }

    currentPlayerLabel_ = new QLabel(QString("-"));
    layout->addWidget(currentPlayerLabel_, playerCount_ + 2, 1);

    actionsLeftLabel_ = new QLabel(QString("-"));
    layout->addWidget(actionsLeftLabel_, playerCount_ + 3, 1);

    weeksLeftLabel_ = new QLabel(QString("-"));
    layout->addWidget(weeksLeftLabel_, playerCount_ + 4, 1);

    primaryBox->setLayout(layout);
    layout_->addWidget(primaryBox);
}

void StatsWidget::setupInfluence()
{
    influenceBox_ = new QGroupBox(this);

    influenceBox_->setAlignment(Qt::AlignHCenter);
    influenceBox_->setTitle("Collected influence cards");

    influenceLayout_ = new QGridLayout();
    influenceBox_->setLayout(influenceLayout_);
    layout_->addWidget(influenceBox_);
}

void StatsWidget::setupText()
{
    QGroupBox *textBox = new QGroupBox(this);
    QVBoxLayout *layout = new QVBoxLayout;

    textLabel0_ = new QLabel("LOADING");
    textLabel1_ = new QLabel("LOADING");
    layout->addWidget(textLabel0_);
    layout->addWidget(textLabel1_);

    textBox->setLayout(layout);
    layout_->addWidget(textBox);
}

void StatsWidget::updateStats()
{
    currentPlayerLabel_->setText(game_->currentPlayer()->name());
    actionsLeftLabel_->setText(QString::number(Bookkeeping::B.actionsLeft()));

    for (unsigned short i = 0; i < playerCount_; i++) {
        cardCountLabels_[i]->setText( QString::number(game_->players()[i]->cards().size()) );
        influenceLabels_[i]->setText( QString::number(Bookkeeping::B.totalInfluence(game_->players()[i])));
    }

    clearWidgets(influenceLayout_);

    unsigned short number = 0;
    for(shared_ptr<OwnedInfluenceCard> influenceCard : Bookkeeping::B.influenceCards(game_->currentPlayer())) {
        QLabel *nameLabel = new QLabel(influenceCard->name());
        QLabel *valueLabel = new QLabel(QString::number(influenceCard->amount()));
        influenceLayout_->addWidget(nameLabel, number, 0);
        influenceLayout_->addWidget(valueLabel, number, 1);
        number++;
    }

    influenceBox_->update();

    int turnsLeftNew = Bookkeeping::B.turnsLeft();
    if ( turnsLeft_ != turnsLeftNew) {
        turnsLeft_ = turnsLeftNew;
        showText(0, QString(" "));
        showText(1, QString(" "));
    }

    weeksLeftLabel_->setText(QString::number(std::ceil(turnsLeftNew / double(playerCount_)) -1));

    /*
    QGridLayout *layout = new QGridLayout;
    unsigned short number = 0;

    for(shared_ptr<OwnedInfluenceCard> influenceCard : Bookkeeping::B.influenceCards(game_->currentPlayer())) {
        bool found = false;
        QPixmap pixmap;

        auto search = Card::picNames_.find(influenceCard->name());
        if (search != Card::picNames_.end()) {
            pixmap = QPixmap(search->second);
            found = true;
        }

        if(found) {
            qDebug() << "found card, drawing icon";
            QLabel *label = new QLabel();
            label->setPixmap(pixmap.scaled(100, 100));
            layout->addWidget(label, 0, 0);
            number++;
        }
    }
    QLayout *oldLayout = influenceBox_->layout();
    influenceBox_->setLayout(layout);
    oldLayout->deleteLater();
    influenceBox_->update();
    */
}

void StatsWidget::showText(const unsigned short &row, const QString &text)
{
    if(row == 0)
        textLabel0_->setText(text);
    else if (row == 1)
        textLabel1_->setText(text);
    else
        qDebug() << "Error in StatsWidget: tried to write to wrong row" << row;
}

// Copied from
// https://stackoverflow.com/questions/22643853/qt-clear-all-widgets-from-inside-a-qwidgets-layout
void StatsWidget::clearWidgets(QLayout * layout) {
   if (! layout)
      return;
   while (auto item = layout->takeAt(0)) {
      delete item->widget();
      clearWidgets(item->layout());
   }
}
