#Includes common configuration for all subdirectory .pro files.
INCLUDEPATH += . ..

TEMPLATE = lib

# The following keeps the generated files at least somewhat separate
# from the source files.
UI_DIR = uics
MOC_DIR = mocs
OBJECTS_DIR = objs

# C++14 is required by the std::make_unique that's recommended by the course
# https://moodle2.tut.fi/mod/assign/view.php?id=295653
# https://herbsutter.com/2013/05/29/gotw-89-solution-smart-pointers/
CONFIG += c++14
QMAKE_CXXFLAGS += -std=c++14
