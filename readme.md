# ElectionGame
A game from 2017 course "Ohjelmoinnin tekniikat"

Licence: MIT

Alpi Tolvanen & Mika Mäki

## Getting it to run
You can grap the pre-built binary from `RELEASE_BUILD`-directory.

Note! The binary is not portable: Move the whole directory with it along with assets-directory.

## Compilation
Install dependencies (Ubuntu 22.04)

```sudo apt install -y qtcreator qtbase5-dev qt5-qmake cmake```

Open `ElectionGame.pro` with QT-creator and compile it with it. You can find the binary from `student/release`.

## Screenshot
![Screenshot of gameplay](screenshot.png "Screenshot")

